namespace CedeSMSDesktop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabServerSettings = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.picKey = new System.Windows.Forms.PictureBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.tabCompose = new System.Windows.Forms.TabPage();
            this.gboSendSMS = new System.Windows.Forms.GroupBox();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.cmbSIM = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblEncPassword = new System.Windows.Forms.Label();
            this.txtEncPassword = new System.Windows.Forms.TextBox();
            this.btnCheckHelp = new System.Windows.Forms.Button();
            this.chkUseEncryption = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSendSMS = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.tabLog = new System.Windows.Forms.TabPage();
            this.lstLog = new System.Windows.Forms.ListBox();
            this.tabGlobalAddressBook = new System.Windows.Forms.TabPage();
            this.grdGAL = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabServerSettings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKey)).BeginInit();
            this.tabCompose.SuspendLayout();
            this.gboSendSMS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabLog.SuspendLayout();
            this.tabGlobalAddressBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabServerSettings);
            this.tabControl1.Controls.Add(this.tabCompose);
            this.tabControl1.Controls.Add(this.tabLog);
            this.tabControl1.Controls.Add(this.tabGlobalAddressBook);
            this.tabControl1.Location = new System.Drawing.Point(12, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(773, 456);
            this.tabControl1.TabIndex = 0;
            // 
            // tabServerSettings
            // 
            this.tabServerSettings.Controls.Add(this.groupBox1);
            this.tabServerSettings.Location = new System.Drawing.Point(4, 22);
            this.tabServerSettings.Name = "tabServerSettings";
            this.tabServerSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabServerSettings.Size = new System.Drawing.Size(765, 430);
            this.tabServerSettings.TabIndex = 0;
            this.tabServerSettings.Text = "Server Login";
            this.tabServerSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.picLoading);
            this.groupBox1.Controls.Add(this.picKey);
            this.groupBox1.Controls.Add(this.lblInfo);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.btnLogout);
            this.groupBox1.Controls.Add(this.btnLogin);
            this.groupBox1.Controls.Add(this.lblServer);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Controls.Add(this.lblPassword);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.lblUsername);
            this.groupBox1.Controls.Add(this.txtUsername);
            this.groupBox1.Location = new System.Drawing.Point(160, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 307);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SMS Server Login";
            // 
            // picLoading
            // 
            this.picLoading.Image = global::CedeSMSDesktop.Properties.Resources.loading;
            this.picLoading.Location = new System.Drawing.Point(382, 227);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(33, 32);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoading.TabIndex = 46;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // picKey
            // 
            this.picKey.Image = global::CedeSMSDesktop.Properties.Resources.key_32x32;
            this.picKey.Location = new System.Drawing.Point(46, 28);
            this.picKey.Name = "picKey";
            this.picKey.Size = new System.Drawing.Size(33, 36);
            this.picKey.TabIndex = 10;
            this.picKey.TabStop = false;
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(105, 28);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(264, 30);
            this.lblInfo.TabIndex = 9;
            this.lblInfo.Text = "Please enter your Logon details to access the specified CedeSMS server.";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(34, 277);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(76, 13);
            this.lblStatus.TabIndex = 8;
            this.lblStatus.Text = "Disconnected.";
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(108, 229);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(89, 25);
            this.btnLogout.TabIndex = 7;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(287, 229);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(91, 25);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(34, 166);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(64, 13);
            this.lblServer.TabIndex = 5;
            this.lblServer.Text = "SMS Server";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(108, 163);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(270, 20);
            this.txtServer.TabIndex = 4;
            this.txtServer.Text = "127.0.0.1";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(43, 122);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(108, 119);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(270, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(43, 80);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(55, 13);
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "Username";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(108, 77);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(270, 20);
            this.txtUsername.TabIndex = 0;
            // 
            // tabCompose
            // 
            this.tabCompose.Controls.Add(this.gboSendSMS);
            this.tabCompose.Location = new System.Drawing.Point(4, 22);
            this.tabCompose.Name = "tabCompose";
            this.tabCompose.Size = new System.Drawing.Size(765, 430);
            this.tabCompose.TabIndex = 2;
            this.tabCompose.Text = "Compose SMS";
            this.tabCompose.UseVisualStyleBackColor = true;
            // 
            // gboSendSMS
            // 
            this.gboSendSMS.Controls.Add(this.lblMobileNumber);
            this.gboSendSMS.Controls.Add(this.cmbSIM);
            this.gboSendSMS.Controls.Add(this.pictureBox2);
            this.gboSendSMS.Controls.Add(this.lblEncPassword);
            this.gboSendSMS.Controls.Add(this.txtEncPassword);
            this.gboSendSMS.Controls.Add(this.btnCheckHelp);
            this.gboSendSMS.Controls.Add(this.chkUseEncryption);
            this.gboSendSMS.Controls.Add(this.label8);
            this.gboSendSMS.Controls.Add(this.btnSendSMS);
            this.gboSendSMS.Controls.Add(this.label9);
            this.gboSendSMS.Controls.Add(this.txtMessage);
            this.gboSendSMS.Location = new System.Drawing.Point(139, 41);
            this.gboSendSMS.Name = "gboSendSMS";
            this.gboSendSMS.Size = new System.Drawing.Size(473, 340);
            this.gboSendSMS.TabIndex = 44;
            this.gboSendSMS.TabStop = false;
            this.gboSendSMS.Text = "Send SMS";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.Location = new System.Drawing.Point(98, 16);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(0, 13);
            this.lblMobileNumber.TabIndex = 50;
            // 
            // cmbSIM
            // 
            this.cmbSIM.FormattingEnabled = true;
            this.cmbSIM.Location = new System.Drawing.Point(101, 38);
            this.cmbSIM.Name = "cmbSIM";
            this.cmbSIM.Size = new System.Drawing.Size(146, 21);
            this.cmbSIM.TabIndex = 49;
            this.cmbSIM.SelectedIndexChanged += new System.EventHandler(this.cmbSIM_SelectedIndexChanged);
            this.cmbSIM.TextChanged += new System.EventHandler(this.cmbSIM_TextChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CedeSMSDesktop.Properties.Resources.loading;
            this.pictureBox2.Location = new System.Drawing.Point(208, 289);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // lblEncPassword
            // 
            this.lblEncPassword.AutoSize = true;
            this.lblEncPassword.Location = new System.Drawing.Point(39, 68);
            this.lblEncPassword.Name = "lblEncPassword";
            this.lblEncPassword.Size = new System.Drawing.Size(56, 13);
            this.lblEncPassword.TabIndex = 48;
            this.lblEncPassword.Text = "Password:";
            this.lblEncPassword.Visible = false;
            // 
            // txtEncPassword
            // 
            this.txtEncPassword.Location = new System.Drawing.Point(101, 65);
            this.txtEncPassword.MaxLength = 15;
            this.txtEncPassword.Name = "txtEncPassword";
            this.txtEncPassword.PasswordChar = '*';
            this.txtEncPassword.Size = new System.Drawing.Size(146, 20);
            this.txtEncPassword.TabIndex = 47;
            this.txtEncPassword.Visible = false;
            // 
            // btnCheckHelp
            // 
            this.btnCheckHelp.Location = new System.Drawing.Point(426, 39);
            this.btnCheckHelp.Name = "btnCheckHelp";
            this.btnCheckHelp.Size = new System.Drawing.Size(19, 20);
            this.btnCheckHelp.TabIndex = 46;
            this.btnCheckHelp.Text = "?";
            this.btnCheckHelp.UseVisualStyleBackColor = true;
            // 
            // chkUseEncryption
            // 
            this.chkUseEncryption.AutoSize = true;
            this.chkUseEncryption.Location = new System.Drawing.Point(284, 42);
            this.chkUseEncryption.Name = "chkUseEncryption";
            this.chkUseEncryption.Size = new System.Drawing.Size(147, 17);
            this.chkUseEncryption.TabIndex = 45;
            this.chkUseEncryption.Text = "Send to CedeSMS clients";
            this.chkUseEncryption.UseVisualStyleBackColor = true;
            this.chkUseEncryption.CheckedChanged += new System.EventHandler(this.chkUseEncryption_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Message";
            // 
            // btnSendSMS
            // 
            this.btnSendSMS.Location = new System.Drawing.Point(377, 297);
            this.btnSendSMS.Name = "btnSendSMS";
            this.btnSendSMS.Size = new System.Drawing.Size(75, 25);
            this.btnSendSMS.TabIndex = 40;
            this.btnSendSMS.Text = "Send";
            this.btnSendSMS.UseVisualStyleBackColor = true;
            this.btnSendSMS.Click += new System.EventHandler(this.btnSendSMS_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Mobile Recipient:";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(17, 115);
            this.txtMessage.MaxLength = 98;
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(434, 168);
            this.txtMessage.TabIndex = 39;
            // 
            // tabLog
            // 
            this.tabLog.Controls.Add(this.lstLog);
            this.tabLog.Location = new System.Drawing.Point(4, 22);
            this.tabLog.Name = "tabLog";
            this.tabLog.Size = new System.Drawing.Size(765, 430);
            this.tabLog.TabIndex = 3;
            this.tabLog.Text = "Log";
            this.tabLog.UseVisualStyleBackColor = true;
            // 
            // lstLog
            // 
            this.lstLog.FormattingEnabled = true;
            this.lstLog.HorizontalScrollbar = true;
            this.lstLog.Location = new System.Drawing.Point(1, 4);
            this.lstLog.Name = "lstLog";
            this.lstLog.Size = new System.Drawing.Size(761, 420);
            this.lstLog.TabIndex = 0;
            // 
            // tabGlobalAddressBook
            // 
            this.tabGlobalAddressBook.Controls.Add(this.grdGAL);
            this.tabGlobalAddressBook.Location = new System.Drawing.Point(4, 22);
            this.tabGlobalAddressBook.Name = "tabGlobalAddressBook";
            this.tabGlobalAddressBook.Size = new System.Drawing.Size(765, 430);
            this.tabGlobalAddressBook.TabIndex = 4;
            this.tabGlobalAddressBook.Text = "Global Address Book";
            this.tabGlobalAddressBook.UseVisualStyleBackColor = true;
            // 
            // grdGAL
            // 
            this.grdGAL.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdGAL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGAL.Location = new System.Drawing.Point(137, 51);
            this.grdGAL.Name = "grdGAL";
            this.grdGAL.Size = new System.Drawing.Size(477, 314);
            this.grdGAL.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CedeSMSDesktop.Properties.Resources.TopBanner;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(799, 68);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 533);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CedeSMS Desktop";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabServerSettings.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKey)).EndInit();
            this.tabCompose.ResumeLayout(false);
            this.gboSendSMS.ResumeLayout(false);
            this.gboSendSMS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabLog.ResumeLayout(false);
            this.tabGlobalAddressBook.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabServerSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TabPage tabCompose;
        private System.Windows.Forms.TabPage tabLog;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.PictureBox picKey;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.GroupBox gboSendSMS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSendSMS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button btnCheckHelp;
        private System.Windows.Forms.CheckBox chkUseEncryption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblEncPassword;
        private System.Windows.Forms.TextBox txtEncPassword;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.TabPage tabGlobalAddressBook;
        private System.Windows.Forms.DataGridView grdGAL;
        private System.Windows.Forms.ComboBox cmbSIM;
        private System.Windows.Forms.Label lblMobileNumber;
    }
}

