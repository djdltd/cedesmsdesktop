using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace CedeSMSDesktop
{
    public partial class Form1 : Form
    {

        byte[] m_dataBuffer = new byte[10];
        IAsyncResult m_result;
        public AsyncCallback m_pfnCallBack;
        public Socket m_clientSocket;

        public String m_receivedXml;

        public String m_username = "";
        public String m_password = "";

        public int m_myClientnumber = 0;

        public delegate void ReceiveLogEvent(string MessageLog);

        public event ReceiveLogEvent OnReceiveLogEvent;

        public delegate void AnalyseDataCallback(String strData);

        public event AnalyseDataCallback AnalyseEvent;

        private List<SingleContactItem> m_globaladdressbook = new List<SingleContactItem>();

        AppSettings m_appsettings = new AppSettings();

        public Form1()
        {
            InitializeComponent();
        }

        void ToggleLogonEnabled(bool bEnabled)
        {
            lblUsername.Enabled = bEnabled;
            lblPassword.Enabled = bEnabled;
            lblServer.Enabled = bEnabled;
            txtUsername.Enabled = bEnabled;
            txtPassword.Enabled = bEnabled;
            txtServer.Enabled = bEnabled;
            btnLogin.Enabled = bEnabled;
            lblInfo.Enabled = bEnabled;
            picKey.Enabled = bEnabled;
            btnLogout.Enabled = !bEnabled;
            //picLoading.Visible = false;

            if (bEnabled == true)
            {
                tabControl1.TabPages.Remove(tabCompose);
                tabControl1.TabPages.Remove(tabGlobalAddressBook);
            }
            else
            {
                tabControl1.TabPages.Add(tabCompose);
                tabControl1.TabPages.Add(tabGlobalAddressBook);
            }
        }

        void ToggleLogonInProgress(bool logoninprogress)
        {
            lblUsername.Enabled = !logoninprogress;
            lblPassword.Enabled = !logoninprogress;
            lblServer.Enabled = !logoninprogress;
            txtUsername.Enabled = !logoninprogress;
            txtPassword.Enabled = !logoninprogress;
            txtServer.Enabled = !logoninprogress;
            btnLogin.Enabled = !logoninprogress;
            lblInfo.Enabled = !logoninprogress;
            picKey.Enabled = !logoninprogress;            
            picLoading.Visible = logoninprogress;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            m_username = txtUsername.Text;
            m_password = txtPassword.Text;

            FormToAppSettings();
            SaveXMLAppSettings();

            // See if we have text on the IP and Port text fields
            if (txtServer.Text.Trim() == "")
            {
                MessageBox.Show("Server Name or IP Address is required to connect to the Server\n");
                return;
            }

            ToggleLogonInProgress(true);

            try
            {                
                // Create the socket instance
                m_clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                

                // Cet the remote IP address
                IPAddress ip = IPAddress.Parse(txtServer.Text);
                
                // Create the end point 
                IPEndPoint ipEnd = new IPEndPoint(ip, 4047);
                // Connect to the remote host
                m_clientSocket.Connect(ipEnd);
                if (m_clientSocket.Connected)
                {
                    ToggleLogonInProgress(false);
                    //Wait for data asynchronously 
                    WaitForData();
                }
            }
            catch (SocketException se)
            {
                string str;
                str = "\nConnection failed, is the server running?\n" + se.Message;
                MessageBox.Show(str);                
            }

            ToggleLogonInProgress(false);
        }



        public void WaitForData()
        {
            try
            {
                if (m_pfnCallBack == null)
                {
                    m_pfnCallBack = new AsyncCallback(OnDataReceived);
                }
                SocketPacket theSocPkt = new SocketPacket();
                theSocPkt.thisSocket = m_clientSocket;
                // Start listening to the data asynchronously
                if (m_clientSocket != null)
                {
                    m_result = m_clientSocket.BeginReceive(theSocPkt.dataBuffer,
                                                            0, theSocPkt.dataBuffer.Length,
                                                            SocketFlags.None,
                                                            m_pfnCallBack,
                                                            theSocPkt);
                }
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }

        }
        public class SocketPacket
        {
            public System.Net.Sockets.Socket thisSocket;
            public byte[] dataBuffer = new byte[1000000];
        }

        public void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                SocketPacket theSockId = (SocketPacket)asyn.AsyncState;
                int iRx = theSockId.thisSocket.EndReceive(asyn);
                char[] chars = new char[iRx + 1];
                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                int charLen = d.GetChars(theSockId.dataBuffer, 0, iRx, chars, 0);
                System.String szData = new System.String(chars);

                //MessageBox.Show(szData);

                //lstLog.Items.Add(szData);
                //MessageBox.Show(szData);
                if (szData.Contains("<MessageType>PING") == false)
                {                    
                    this.Invoke(AnalyseEvent, szData);
                }

                

                WaitForData();
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
        }

        public static object FromXml(string Xml, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType);
            StringReader stringReader;
            stringReader = new StringReader(Xml);
            XmlTextReader xmlReader;
            xmlReader = new XmlTextReader(stringReader);
            object obj;
            obj = ser.Deserialize(xmlReader);
            xmlReader.Close();
            stringReader.Close();
            return obj;

        }

        public static string ToXml(object Obj, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType);
            MemoryStream memStream;
            memStream = new MemoryStream();
            XmlTextWriter xmlWriter;
            xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
            xmlWriter.Namespaces = true;
            ser.Serialize(xmlWriter, Obj);
            xmlWriter.Close();
            memStream.Close();
            string xml;
            xml = Encoding.UTF8.GetString(memStream.GetBuffer());
            xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
            xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1));
            return xml;

        }

        private void AnalyseMessage(String strXml)
        {
            CommPacket rcvPacket = new CommPacket();

            rcvPacket = (CommPacket) FromXml(strXml, typeof(CommPacket));
            

            if (rcvPacket.MessageType == "NEEDAUTHENTICATION")
            {
                m_myClientnumber = rcvPacket.ClientNumber;

                //MessageBox.Show("need to send auth.");
                CommPacket sendpacket = new CommPacket();
                sendpacket.MessageType = "AUTHENTICATION";
                sendpacket.ClientNumber = m_myClientnumber;
                sendpacket.Username = m_username;
                sendpacket.Password = m_password;

                SendToServer(sendpacket);

                //MessageBox.Show("auth sent!");
            }



            if (rcvPacket.MessageType == "AUTHENTICATIONOK")
            {
                //MessageBox.Show("Access Granted.");
                ToggleLogonEnabled(false);
                lblStatus.Text = "Successfully logged on to " + txtServer.Text;

                if (rcvPacket.GALPresent == true)
                {
                    m_globaladdressbook = (List<SingleContactItem>)FromXml(rcvPacket.GALData, typeof(List<SingleContactItem>));
                    RefreshGlobalAddressBook();
                }
            }

            if (rcvPacket.MessageType == "AUTHENTICATIONDENIED")
            {
                DisconnectFromServer();
                lblStatus.Text = "Logon denied.";
                MessageBox.Show("Access Denied.");                
            }

            if (rcvPacket.MessageType == "SERVERFULL")
            {
                DisconnectFromServer();
                lblStatus.Text = "Server full, please try again later.";
                MessageBox.Show("Server reports it has reached the maximum number of connected clients. Please try again later.");
                
            }

            if (rcvPacket.MessageType == "SENTSMSOK")
            {
                ToggleSendingGUI(false);
                MessageBox.Show("SMS was relayed successfully.");
            }

            if (rcvPacket.MessageType == "SENTSMSFAILED")
            {
                ToggleSendingGUI(false);
                MessageBox.Show("SMS relay failed.");
            }
        }

        private void SendToServer(CommPacket packet)
        {
            String strXml = ToXml(packet, typeof(CommPacket));

            try
            {
                Object objData = strXml;
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(objData.ToString());
                if (m_clientSocket != null)
                {
                    m_clientSocket.Send(byData);
                }
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
        }

        private void DisconnectFromServer()
        {
            if (m_clientSocket != null)
            {
                m_clientSocket.Shutdown(SocketShutdown.Both);
                m_clientSocket.Close();
                m_clientSocket = null;                
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OnReceiveLogEvent += new ReceiveLogEvent(Form1_OnReceiveLogEvent);
            AnalyseEvent += new AnalyseDataCallback(Form1_AnalyseEvent);

            LoadXMLAppSettings();
            AppSettingsToForm();

            ToggleLogonEnabled(true);
        }

        void Form1_AnalyseEvent(string strData)
        {
            //throw new Exception("The method or operation is not implemented.");
            OnReceiveLogEvent(strData);
            AnalyseMessage(strData);
        }

        private void RefreshGlobalAddressBook()
        {
            grdGAL.Columns.Clear();

            grdGAL.Columns.Add("Name", "Name");
            grdGAL.Columns.Add("Mobile", "Mobile");
            grdGAL.Columns.Add("Group", "Group");

            for (int c = 0; c < m_globaladdressbook.Count; c++)
            {
                SingleContactItem current = new SingleContactItem();
                current = (SingleContactItem)m_globaladdressbook[c];

                grdGAL.Rows.Add();

                grdGAL.Rows[c].Cells[0].Value = current.Name;
                grdGAL.Rows[c].Cells[1].Value = current.MobileNumber;
                grdGAL.Rows[c].Cells[2].Value = current.GroupName;

                cmbSIM.Items.Add(current.Name);
            }
        }

        private String LookupMobileNumberFromGAL(String strName)
        {
            for (int c = 0; c < m_globaladdressbook.Count; c++)
            {
                SingleContactItem current = new SingleContactItem();
                current = (SingleContactItem)m_globaladdressbook[c];

                if (current.Name.ToUpper() == strName.ToUpper())
                {
                    return current.MobileNumber;
                }
            }
            return "";
        }

        void Form1_OnReceiveLogEvent(string MessageLog)
        {
            //throw new Exception("The method or operation is not implemented.");
            lstLog.Items.Add(MessageLog);

            
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            DisconnectFromServer();
            ToggleLogonEnabled(true);
            lblStatus.Text = "Disconnected.";
        }

        private bool IsMobileNumber(String strText)
        {
            return true;
        }

        private void btnSendSMS_Click(object sender, EventArgs e)
        {
            if (cmbSIM.Text.Trim() == "")
            {
                MessageBox.Show("You must enter a valid mobile number using International format (e.g. +441234321432)");
                return;
            }

            if (txtMessage.Text.Trim() == "")
            {
                MessageBox.Show("You must enter a message to send.");
                return;
            }

            if (chkUseEncryption.Checked == true)
            {
                if (txtEncPassword.Text.Trim() == "")
                {
                    MessageBox.Show("You must enter a password to send encrypted messages.");
                    return;
                }
            }

            CommPacket packet = new CommPacket();

            packet.MessageType = "SENDSMS";
            packet.UseEncryption = chkUseEncryption.Checked;
            packet.PhoneNumber = lblMobileNumber.Text;
            packet.Message = txtMessage.Text;
            packet.Password = txtEncPassword.Text;
            packet.Username = txtUsername.Text;
            ToggleSendingGUI(true);

            SendToServer(packet);
        }

        private void ToggleSendingGUI(bool sendinprogress)
        {
            label9.Enabled = !sendinprogress;
            lblEncPassword.Enabled = !sendinprogress;
            cmbSIM.Enabled = !sendinprogress;
            txtEncPassword.Enabled = !sendinprogress;            
            chkUseEncryption.Enabled = !sendinprogress;
            btnCheckHelp.Enabled = !sendinprogress;
            label8.Enabled = !sendinprogress;
            txtMessage.Enabled = !sendinprogress;
            btnSendSMS.Enabled = !sendinprogress;
            lblMobileNumber.Enabled = !sendinprogress;
            pictureBox2.Visible = sendinprogress;
        }

        private void chkUseEncryption_CheckedChanged(object sender, EventArgs e)
        {
            lblEncPassword.Visible = chkUseEncryption.Checked;
            txtEncPassword.Visible = chkUseEncryption.Checked;
        }

        private void cmbSIM_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMobileNumber.Text = LookupMobileNumberFromGAL(cmbSIM.Text);
        }

        private void cmbSIM_TextChanged(object sender, EventArgs e)
        {
            if (cmbSIM.Text.Length > 1)
            {
                if (cmbSIM.Text.StartsWith("0"))
                {

                    lblMobileNumber.Text = "+44" + cmbSIM.Text.Substring(1).Replace(" ", "");
                }
                else
                {
                    lblMobileNumber.Text = cmbSIM.Text.Replace(" ", "");
                }
            }            
        }

        private String GetAppSettingsDir()
        {
            // Function to retrieve the applications settings directory which
            // is usually APPDATA\\FileCopyProgress
            String strSavepath = System.Environment.GetEnvironmentVariable("APPDATA") + "\\CedeSMSDesktop";
            Directory.CreateDirectory(strSavepath);
            return strSavepath + "\\";
        }

        public void LoadXMLAppSettings()
        {
            if (File.Exists(GetAppSettingsDir() + "Settings.xml") == true)
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(AppSettings));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(GetAppSettingsDir() + "Settings.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                m_appsettings = (AppSettings)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        public void SaveXMLAppSettings()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(AppSettings));
            StreamWriter myWriter = new StreamWriter(GetAppSettingsDir() + "Settings.xml");

            mySerializer.Serialize(myWriter, m_appsettings);
            myWriter.Close();
        }

        public void AppSettingsToForm()
        {
            txtUsername.Text = m_appsettings.Username;
            txtPassword.Text = m_appsettings.Password;
            txtServer.Text = m_appsettings.Server;
        }

        public void FormToAppSettings()
        {
            m_appsettings.Username = txtUsername.Text;
            m_appsettings.Password = txtPassword.Text;
            m_appsettings.Server = txtServer.Text;
        }
    }

    public class AppSettings
    {
        public String Username;
        public String Password;
        public String Server;

    }
}