using System;
using System.Collections.Generic;
using System.Text;

namespace CedeSMSDesktop
{
    public class SingleContactItem
    {
        public String Name;
        public String MobileNumber;
        public String GroupName;

        public SingleContactItem(String name, String mobilenumber, String groupname)
        {
            Name = name;
            MobileNumber = mobilenumber;
            GroupName = groupname;
        }

        public SingleContactItem()
        {

        }
    }
}
