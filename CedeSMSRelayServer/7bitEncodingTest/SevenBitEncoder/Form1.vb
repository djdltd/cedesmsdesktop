Public Class Form1

    Shared Function Encode7Bit(ByVal Content As String) As String
        'Prepare
        Dim CharArray As Char() = Content.ToCharArray
        Dim c As Char
        Dim t As String
        For Each c In CharArray
            t = CharTo7Bits(c) + t
        Next
        'Add "0"
        Dim i As Integer
        If (t.Length Mod 8) <> 0 Then
            For i = 1 To 8 - (t.Length Mod 8)
                t = "0" + t
            Next
        End If
        'Split into 8bits
        Dim result As String
        Dim cur As String
        For i = t.Length - 8 To 0 Step -8
            cur = Mid(t, i + 1, 8)
            result = result + BitsToHex(Mid(t, i + 1, 8))
        Next
        Return result
    End Function

    Shared Function BitsToHex(ByVal Bits As String) As String
        'Convert 8Bits to Hex String
        Dim i, v As Integer
        For i = 0 To Bits.Length - 1
            v = v + Val(Mid(Bits, i + 1, 1)) * 2 ^ (7 - i)
        Next
        Dim result As String
        result = Format(v, "X2")

        'result = v.ToString()
        Return result
    End Function

    Shared Function CharTo7Bits(ByVal c As Char) As String
        If c = "@" Then Return "0000000"
        Dim Result As String
        Dim i As Integer
        For i = 0 To 6
            If (Asc(c) And 2 ^ i) > 0 Then
                Result = "1" + Result
            Else
                Result = "0" + Result
            End If
        Next
        Return Result
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEncode.Click
        'txtEncoded.Text = Encode7Bit("It is easy to send text messages.")


        'Dim strTest As String = "a"
        MessageBox.Show(Encode7Bit("It is easy to send text messages."))

        'MessageBox.Show(BitsToHex("00011000"))

    End Sub
End Class
