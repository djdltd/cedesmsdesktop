<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnEncode = New System.Windows.Forms.Button
        Me.txtEncoded = New System.Windows.Forms.TextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnEncode
        '
        Me.btnEncode.Location = New System.Drawing.Point(30, 31)
        Me.btnEncode.Name = "btnEncode"
        Me.btnEncode.Size = New System.Drawing.Size(93, 46)
        Me.btnEncode.TabIndex = 0
        Me.btnEncode.Text = "Encode 7 bit"
        Me.btnEncode.UseVisualStyleBackColor = True
        '
        'txtEncoded
        '
        Me.txtEncoded.Location = New System.Drawing.Point(88, 83)
        Me.txtEncoded.Name = "txtEncoded"
        Me.txtEncoded.Size = New System.Drawing.Size(643, 20)
        Me.txtEncoded.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(10, 86)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(72, 13)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "7 bit encoded"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 264)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtEncoded)
        Me.Controls.Add(Me.btnEncode)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnEncode As System.Windows.Forms.Button
    Friend WithEvents txtEncoded As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label

End Class
