using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SMSapplication
{
    public partial class ManageableGrid : UserControl
    {
        private ArrayList _objectlist = new ArrayList();
        private Object _blankitem = new Object();
        private Type _objecttype;
        private int _lastindex;

        public delegate void ListChanged ();
        public event ListChanged OnListChanged;

        public ManageableGrid()
        {
            InitializeComponent();
            
        }

        public ArrayList ObjectList
        {
            get
            {
                return _objectlist;
            }
        }

        public void Populate(ArrayList objectlist, Type ObjectType, Object ObjectItem)
        {
            grdObjects.Columns.Clear();
            _blankitem = ObjectItem;
            _objectlist = objectlist;
            _objecttype = ObjectType;
            int p = 0;
            Type t = ObjectType;
            PropertyInfo[] pinfo = t.GetProperties();

            for (p = 0; p < pinfo.Length; p++)
            {
                PropertyInfo pi = pinfo[p];
                grdObjects.Columns.Add(pi.Name, pi.Name);
            }

            for (int u = 0; u < objectlist.Count; u++)
            {
                Object citem = new Object();
                citem = objectlist[u];

                grdObjects.Rows.Add();

                for (p = 0; p < pinfo.Length; p++)
                {
                    PropertyInfo pi = pinfo[p];
                    Object obj = pi.GetValue(citem, null);
                    grdObjects.Rows[u].Cells[p].Value = obj.ToString();
                }
            }
        }

        private void RefreshGrid()
        {
            grdObjects.Columns.Clear();
            int p = 0;
            Type t = _blankitem.GetType();
            PropertyInfo[] pinfo = t.GetProperties();

            for (p = 0; p < pinfo.Length; p++)
            {
                PropertyInfo pi = pinfo[p];
                grdObjects.Columns.Add(pi.Name, pi.Name);
            }

            for (int u = 0; u < _objectlist.Count; u++)
            {
                Object citem = new Object();
                citem = _objectlist[u];

                grdObjects.Rows.Add();

                for (p = 0; p < pinfo.Length; p++)
                {
                    PropertyInfo pi = pinfo[p];
                    Object obj = pi.GetValue(citem, null);
                    grdObjects.Rows[u].Cells[p].Value = obj.ToString();
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Object item = new Object();

            if (_objecttype == typeof(AuthUser))
            {
                item = new AuthUser();
            }

            if (_objecttype == typeof(SingleContactItem))
            {
                item = new SingleContactItem();
            }

            NewItem newItemdialog = new NewItem();
            newItemdialog.OnFormCompletedEvent += new NewItem.FormCompleted(newItemdialog_OnFormCompletedEvent);

            newItemdialog.Populate(item, _objecttype);
            newItemdialog.Show();

        }

        void newItemdialog_OnFormCompletedEvent(object userData)
        {
            //MessageBox.Show("Data entered");
            Object item = new Object();
            
            item = userData;
            ArrayList temp = new ArrayList ();
            temp = (ArrayList) _objectlist.Clone();

            temp.Add(item);

            _objectlist = temp;

            RefreshGrid();

            if (OnListChanged != null)
            {
                OnListChanged();
            }
            //throw new Exception("The method or operation is not implemented.");
        }

        private void grdObjects_DoubleClick(object sender, EventArgs e)
        {
            int index = grdObjects.SelectedCells[0].RowIndex;
            _lastindex = index;



            Object item = new Object();
            item = _objectlist[index];

            NewItem itemDialog = new NewItem();
            itemDialog.OnFormCompletedEvent += new NewItem.FormCompleted(itemDialog_OnFormCompletedEvent);

            itemDialog.Populate(item, _objecttype);
            itemDialog.Show();
        }

        void itemDialog_OnFormCompletedEvent(object userData)
        {
            _objectlist[_lastindex] = userData;

            RefreshGrid();

            if (OnListChanged != null)
            {
                OnListChanged();
            }

            
            //throw new Exception("The method or operation is not implemented.");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = grdObjects.SelectedCells[0].RowIndex;

            if (MessageBox.Show("Delete item?", "Delete Item", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _objectlist.RemoveAt(index);
                RefreshGrid();

                if (OnListChanged != null)
                {
                    OnListChanged();
                }
            }
        }


    }
}
