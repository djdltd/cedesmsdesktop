using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

namespace SMSapplication
{
    public partial class NewItem : Form
    {
        public delegate void FormCompleted(Object userData);
        public event FormCompleted OnFormCompletedEvent;

        private Object _formitem = new Object();

        public NewItem()
        {
            InitializeComponent();
        }


        /*
                 private void RefreshUsers()
        {
            int p = 0;
            Type t = typeof(AuthUser);
            PropertyInfo[] pinfo = t.GetProperties();

            for (p = 0; p < pinfo.Length; p++)
            {
                PropertyInfo pi = pinfo[p];
                grdUsers.Columns.Add(pi.Name, pi.Name);
            }

            for (int u = 0; u < m_authenticatedusers.Count; u++)
            {
                AuthUser cuser = new AuthUser();
                cuser = (AuthUser) m_authenticatedusers[u];

                grdUsers.Rows.Add ();

                for (p = 0; p < pinfo.Length; p++)
                {
                    PropertyInfo pi = pinfo[p];                    
                    Object obj = pi.GetValue(cuser, null);                    
                    grdUsers.Rows[u].Cells[p].Value = obj.ToString();
                }                                                
            }
        }
         */

        public void Populate(Object obj, Type type)
        {
            _formitem = obj;
            int p = 0;
            Type t = type;

            PropertyInfo[] pinfo = t.GetProperties();

            int iStartingheight = 150;

            for (p = 0; p < pinfo.Length; p++)
            {
                PropertyInfo pi = pinfo[p];
                EditField ef = new EditField();

                ef.FieldName = pi.Name;
                ef.FieldValue = (String) pi.GetValue(obj, null);

                flowLayoutPanel1.Controls.Add(ef);                

                iStartingheight += 36;
                this.Height = iStartingheight;                
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Object item = new Object();
            item = _formitem;
            int p = 0;
            int c = 0;
            Type t = item.GetType();

            PropertyInfo[] pinfo = t.GetProperties();

            for (p = 0; p < pinfo.Length; p++)
            {
                PropertyInfo pi = pinfo[p];

                for (c = 0; c < flowLayoutPanel1.Controls.Count; c++)
                {
                    if (flowLayoutPanel1.Controls[c].GetType() == typeof(EditField))
                    {
                        EditField ef = (EditField) flowLayoutPanel1.Controls[c];

                        if (ef.FieldName == pi.Name)
                        {
                            pi.SetValue(item, ef.FieldValue, null);
                        }
                    }
                }
            }

            // trigger the updated event
            OnFormCompletedEvent(item);

            this.Close();
        }
    }
}