namespace SMSapplication
{
    partial class SMSapplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SMSapplication));
            this.tabSMSapplication = new System.Windows.Forms.TabControl();
            this.tbPortSettings = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gboPortSettings = new System.Windows.Forms.GroupBox();
            this.chkConnectOnStartup = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtWriteTimeOut = new System.Windows.Forms.TextBox();
            this.txtReadTimeOut = new System.Windows.Forms.TextBox();
            this.cboParityBits = new System.Windows.Forms.ComboBox();
            this.cboStopBits = new System.Windows.Forms.ComboBox();
            this.cboDataBits = new System.Windows.Forms.ComboBox();
            this.cboBaudRate = new System.Windows.Forms.ComboBox();
            this.cboPortName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSendSMS = new System.Windows.Forms.TabPage();
            this.gboSendSMS = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSendSMS = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSIM = new System.Windows.Forms.TextBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.tbReadSMS = new System.Windows.Forms.TabPage();
            this.gboReadSMS = new System.Windows.Forms.GroupBox();
            this.btnReadSMS = new System.Windows.Forms.Button();
            this.lvwMessages = new System.Windows.Forms.ListView();
            this.colIndex = new System.Windows.Forms.ColumnHeader();
            this.colSender = new System.Windows.Forms.ColumnHeader();
            this.colMessage = new System.Windows.Forms.ColumnHeader();
            this.tbDeleteSMS = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCountSMS = new System.Windows.Forms.Button();
            this.txtCountSMS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gboDeleteSMS = new System.Windows.Forms.GroupBox();
            this.rbDeleteReadSMS = new System.Windows.Forms.RadioButton();
            this.btnDeleteSMS = new System.Windows.Forms.Button();
            this.rbDeleteAllSMS = new System.Windows.Forms.RadioButton();
            this.tabGSMLog = new System.Windows.Forms.TabPage();
            this.txtModemlog = new System.Windows.Forms.TextBox();
            this.tabServerLog = new System.Windows.Forms.TabPage();
            this.lstServerLog = new System.Windows.Forms.ListBox();
            this.tabSMSLog = new System.Windows.Forms.TabPage();
            this.grdSMSLog = new System.Windows.Forms.DataGridView();
            this.tabUsers = new System.Windows.Forms.TabPage();
            this.managedGrid1 = new ManageableGrid();
            this.tabGlobalAddressBook = new System.Windows.Forms.TabPage();
            this.manageableGrid1 = new ManageableGrid();
            this.gboConnectionStatus = new System.Windows.Forms.GroupBox();
            this.lblNumClients = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblConnectionStatus = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnPDUtest = new System.Windows.Forms.Button();
            this.txtPDUOutput = new System.Windows.Forms.TextBox();
            this.lblPDUOutput = new System.Windows.Forms.Label();
            this.lblLengthTitle = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.btnEncode7bit = new System.Windows.Forms.Button();
            this.btnListen = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnSerialise = new System.Windows.Forms.Button();
            this.tmrSocketCleanup = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabSMSapplication.SuspendLayout();
            this.tbPortSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gboPortSettings.SuspendLayout();
            this.tbSendSMS.SuspendLayout();
            this.gboSendSMS.SuspendLayout();
            this.tbReadSMS.SuspendLayout();
            this.gboReadSMS.SuspendLayout();
            this.tbDeleteSMS.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gboDeleteSMS.SuspendLayout();
            this.tabGSMLog.SuspendLayout();
            this.tabServerLog.SuspendLayout();
            this.tabSMSLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMSLog)).BeginInit();
            this.tabUsers.SuspendLayout();
            this.tabGlobalAddressBook.SuspendLayout();
            this.gboConnectionStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabSMSapplication
            // 
            this.tabSMSapplication.Controls.Add(this.tbPortSettings);
            this.tabSMSapplication.Controls.Add(this.tbSendSMS);
            this.tabSMSapplication.Controls.Add(this.tbReadSMS);
            this.tabSMSapplication.Controls.Add(this.tbDeleteSMS);
            this.tabSMSapplication.Controls.Add(this.tabGSMLog);
            this.tabSMSapplication.Controls.Add(this.tabServerLog);
            this.tabSMSapplication.Controls.Add(this.tabSMSLog);
            this.tabSMSapplication.Controls.Add(this.tabUsers);
            this.tabSMSapplication.Controls.Add(this.tabGlobalAddressBook);
            this.tabSMSapplication.Location = new System.Drawing.Point(12, 87);
            this.tabSMSapplication.Name = "tabSMSapplication";
            this.tabSMSapplication.SelectedIndex = 0;
            this.tabSMSapplication.Size = new System.Drawing.Size(755, 406);
            this.tabSMSapplication.TabIndex = 0;
            // 
            // tbPortSettings
            // 
            this.tbPortSettings.Controls.Add(this.pictureBox1);
            this.tbPortSettings.Controls.Add(this.gboPortSettings);
            this.tbPortSettings.Location = new System.Drawing.Point(4, 22);
            this.tbPortSettings.Name = "tbPortSettings";
            this.tbPortSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tbPortSettings.Size = new System.Drawing.Size(747, 380);
            this.tbPortSettings.TabIndex = 0;
            this.tbPortSettings.Text = "Port Settings";
            this.tbPortSettings.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CedeSMSServer.Properties.Resources.TowerPic;
            this.pictureBox1.Location = new System.Drawing.Point(25, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(131, 173);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // gboPortSettings
            // 
            this.gboPortSettings.Controls.Add(this.chkConnectOnStartup);
            this.gboPortSettings.Controls.Add(this.btnOK);
            this.gboPortSettings.Controls.Add(this.txtWriteTimeOut);
            this.gboPortSettings.Controls.Add(this.txtReadTimeOut);
            this.gboPortSettings.Controls.Add(this.cboParityBits);
            this.gboPortSettings.Controls.Add(this.cboStopBits);
            this.gboPortSettings.Controls.Add(this.cboDataBits);
            this.gboPortSettings.Controls.Add(this.cboBaudRate);
            this.gboPortSettings.Controls.Add(this.cboPortName);
            this.gboPortSettings.Controls.Add(this.label7);
            this.gboPortSettings.Controls.Add(this.label6);
            this.gboPortSettings.Controls.Add(this.label5);
            this.gboPortSettings.Controls.Add(this.label4);
            this.gboPortSettings.Controls.Add(this.label3);
            this.gboPortSettings.Controls.Add(this.label2);
            this.gboPortSettings.Controls.Add(this.label1);
            this.gboPortSettings.Location = new System.Drawing.Point(172, 47);
            this.gboPortSettings.Name = "gboPortSettings";
            this.gboPortSettings.Size = new System.Drawing.Size(375, 275);
            this.gboPortSettings.TabIndex = 0;
            this.gboPortSettings.TabStop = false;
            this.gboPortSettings.Text = "GSM Hardware Port Settings";
            // 
            // chkConnectOnStartup
            // 
            this.chkConnectOnStartup.AutoSize = true;
            this.chkConnectOnStartup.Location = new System.Drawing.Point(156, 219);
            this.chkConnectOnStartup.Name = "chkConnectOnStartup";
            this.chkConnectOnStartup.Size = new System.Drawing.Size(118, 17);
            this.chkConnectOnStartup.TabIndex = 15;
            this.chkConnectOnStartup.Text = "Connect on Startup";
            this.chkConnectOnStartup.UseVisualStyleBackColor = true;
            this.chkConnectOnStartup.CheckedChanged += new System.EventHandler(this.chkConnectOnStartup_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(155, 240);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "Connect";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtWriteTimeOut
            // 
            this.txtWriteTimeOut.Location = new System.Drawing.Point(156, 193);
            this.txtWriteTimeOut.MaxLength = 5;
            this.txtWriteTimeOut.Name = "txtWriteTimeOut";
            this.txtWriteTimeOut.Size = new System.Drawing.Size(121, 20);
            this.txtWriteTimeOut.TabIndex = 13;
            this.txtWriteTimeOut.Text = "300";
            // 
            // txtReadTimeOut
            // 
            this.txtReadTimeOut.Location = new System.Drawing.Point(156, 167);
            this.txtReadTimeOut.MaxLength = 5;
            this.txtReadTimeOut.Name = "txtReadTimeOut";
            this.txtReadTimeOut.Size = new System.Drawing.Size(121, 20);
            this.txtReadTimeOut.TabIndex = 12;
            this.txtReadTimeOut.Text = "300";
            // 
            // cboParityBits
            // 
            this.cboParityBits.FormattingEnabled = true;
            this.cboParityBits.Items.AddRange(new object[] {
            "Even",
            "Odd",
            "None"});
            this.cboParityBits.Location = new System.Drawing.Point(156, 140);
            this.cboParityBits.Name = "cboParityBits";
            this.cboParityBits.Size = new System.Drawing.Size(121, 21);
            this.cboParityBits.TabIndex = 11;
            this.cboParityBits.Text = "None";
            // 
            // cboStopBits
            // 
            this.cboStopBits.FormattingEnabled = true;
            this.cboStopBits.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.cboStopBits.Location = new System.Drawing.Point(156, 113);
            this.cboStopBits.Name = "cboStopBits";
            this.cboStopBits.Size = new System.Drawing.Size(121, 21);
            this.cboStopBits.TabIndex = 10;
            this.cboStopBits.Text = "1";
            // 
            // cboDataBits
            // 
            this.cboDataBits.FormattingEnabled = true;
            this.cboDataBits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.cboDataBits.Location = new System.Drawing.Point(156, 86);
            this.cboDataBits.Name = "cboDataBits";
            this.cboDataBits.Size = new System.Drawing.Size(121, 21);
            this.cboDataBits.TabIndex = 9;
            this.cboDataBits.Text = "8";
            // 
            // cboBaudRate
            // 
            this.cboBaudRate.FormattingEnabled = true;
            this.cboBaudRate.Items.AddRange(new object[] {
            "110",
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.cboBaudRate.Location = new System.Drawing.Point(156, 58);
            this.cboBaudRate.Name = "cboBaudRate";
            this.cboBaudRate.Size = new System.Drawing.Size(121, 21);
            this.cboBaudRate.TabIndex = 8;
            this.cboBaudRate.Text = "9600";
            // 
            // cboPortName
            // 
            this.cboPortName.FormattingEnabled = true;
            this.cboPortName.Location = new System.Drawing.Point(156, 31);
            this.cboPortName.Name = "cboPortName";
            this.cboPortName.Size = new System.Drawing.Size(121, 21);
            this.cboPortName.TabIndex = 7;
            this.cboPortName.Text = "COM1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Write Timeout";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(65, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Read Timeout";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Parity Bits";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Stop Bits";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Bits";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Baud Rate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port Name";
            // 
            // tbSendSMS
            // 
            this.tbSendSMS.Controls.Add(this.gboSendSMS);
            this.tbSendSMS.Location = new System.Drawing.Point(4, 22);
            this.tbSendSMS.Name = "tbSendSMS";
            this.tbSendSMS.Padding = new System.Windows.Forms.Padding(3);
            this.tbSendSMS.Size = new System.Drawing.Size(747, 380);
            this.tbSendSMS.TabIndex = 1;
            this.tbSendSMS.Text = "Send SMS";
            this.tbSendSMS.UseVisualStyleBackColor = true;
            // 
            // gboSendSMS
            // 
            this.gboSendSMS.Controls.Add(this.label8);
            this.gboSendSMS.Controls.Add(this.btnSendSMS);
            this.gboSendSMS.Controls.Add(this.label9);
            this.gboSendSMS.Controls.Add(this.txtSIM);
            this.gboSendSMS.Controls.Add(this.txtMessage);
            this.gboSendSMS.Location = new System.Drawing.Point(165, 51);
            this.gboSendSMS.Name = "gboSendSMS";
            this.gboSendSMS.Size = new System.Drawing.Size(375, 275);
            this.gboSendSMS.TabIndex = 43;
            this.gboSendSMS.TabStop = false;
            this.gboSendSMS.Text = "Send SMS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Message";
            // 
            // btnSendSMS
            // 
            this.btnSendSMS.Location = new System.Drawing.Point(283, 223);
            this.btnSendSMS.Name = "btnSendSMS";
            this.btnSendSMS.Size = new System.Drawing.Size(75, 25);
            this.btnSendSMS.TabIndex = 40;
            this.btnSendSMS.Text = "Send";
            this.btnSendSMS.UseVisualStyleBackColor = true;
            this.btnSendSMS.Click += new System.EventHandler(this.btnSendSMS_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Mobile Number:";
            // 
            // txtSIM
            // 
            this.txtSIM.Location = new System.Drawing.Point(102, 38);
            this.txtSIM.MaxLength = 15;
            this.txtSIM.Name = "txtSIM";
            this.txtSIM.Size = new System.Drawing.Size(118, 20);
            this.txtSIM.TabIndex = 41;
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(17, 107);
            this.txtMessage.MaxLength = 160;
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(341, 100);
            this.txtMessage.TabIndex = 39;
            // 
            // tbReadSMS
            // 
            this.tbReadSMS.Controls.Add(this.gboReadSMS);
            this.tbReadSMS.Location = new System.Drawing.Point(4, 22);
            this.tbReadSMS.Name = "tbReadSMS";
            this.tbReadSMS.Padding = new System.Windows.Forms.Padding(3);
            this.tbReadSMS.Size = new System.Drawing.Size(747, 380);
            this.tbReadSMS.TabIndex = 2;
            this.tbReadSMS.Text = "Read SMS";
            this.tbReadSMS.UseVisualStyleBackColor = true;
            // 
            // gboReadSMS
            // 
            this.gboReadSMS.Controls.Add(this.btnReadSMS);
            this.gboReadSMS.Controls.Add(this.lvwMessages);
            this.gboReadSMS.Location = new System.Drawing.Point(19, 10);
            this.gboReadSMS.Name = "gboReadSMS";
            this.gboReadSMS.Size = new System.Drawing.Size(375, 275);
            this.gboReadSMS.TabIndex = 43;
            this.gboReadSMS.TabStop = false;
            this.gboReadSMS.Text = "Read SMS";
            // 
            // btnReadSMS
            // 
            this.btnReadSMS.Location = new System.Drawing.Point(155, 240);
            this.btnReadSMS.Name = "btnReadSMS";
            this.btnReadSMS.Size = new System.Drawing.Size(75, 25);
            this.btnReadSMS.TabIndex = 41;
            this.btnReadSMS.Text = "Read";
            this.btnReadSMS.UseVisualStyleBackColor = true;
            this.btnReadSMS.Click += new System.EventHandler(this.btnReadSMS_Click);
            // 
            // lvwMessages
            // 
            this.lvwMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwMessages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIndex,
            this.colSender,
            this.colMessage});
            this.lvwMessages.FullRowSelect = true;
            this.lvwMessages.Location = new System.Drawing.Point(6, 19);
            this.lvwMessages.MultiSelect = false;
            this.lvwMessages.Name = "lvwMessages";
            this.lvwMessages.Size = new System.Drawing.Size(352, 192);
            this.lvwMessages.TabIndex = 38;
            this.lvwMessages.UseCompatibleStateImageBehavior = false;
            this.lvwMessages.View = System.Windows.Forms.View.Details;
            // 
            // colIndex
            // 
            this.colIndex.Text = "Index";
            this.colIndex.Width = 43;
            // 
            // colSender
            // 
            this.colSender.Text = "Sender";
            this.colSender.Width = 50;
            // 
            // colMessage
            // 
            this.colMessage.Text = "Message";
            this.colMessage.Width = 275;
            // 
            // tbDeleteSMS
            // 
            this.tbDeleteSMS.Controls.Add(this.groupBox1);
            this.tbDeleteSMS.Controls.Add(this.gboDeleteSMS);
            this.tbDeleteSMS.Location = new System.Drawing.Point(4, 22);
            this.tbDeleteSMS.Name = "tbDeleteSMS";
            this.tbDeleteSMS.Padding = new System.Windows.Forms.Padding(3);
            this.tbDeleteSMS.Size = new System.Drawing.Size(747, 380);
            this.tbDeleteSMS.TabIndex = 3;
            this.tbDeleteSMS.Text = "Delete SMS";
            this.tbDeleteSMS.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCountSMS);
            this.groupBox1.Controls.Add(this.txtCountSMS);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(23, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 96);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Count SMS";
            // 
            // btnCountSMS
            // 
            this.btnCountSMS.Location = new System.Drawing.Point(149, 55);
            this.btnCountSMS.Name = "btnCountSMS";
            this.btnCountSMS.Size = new System.Drawing.Size(75, 25);
            this.btnCountSMS.TabIndex = 46;
            this.btnCountSMS.Text = "Count";
            this.btnCountSMS.UseVisualStyleBackColor = true;
            this.btnCountSMS.Click += new System.EventHandler(this.btnCountSMS_Click);
            // 
            // txtCountSMS
            // 
            this.txtCountSMS.Location = new System.Drawing.Point(80, 26);
            this.txtCountSMS.Name = "txtCountSMS";
            this.txtCountSMS.ReadOnly = true;
            this.txtCountSMS.Size = new System.Drawing.Size(34, 20);
            this.txtCountSMS.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Count SMS";
            // 
            // gboDeleteSMS
            // 
            this.gboDeleteSMS.Controls.Add(this.rbDeleteReadSMS);
            this.gboDeleteSMS.Controls.Add(this.btnDeleteSMS);
            this.gboDeleteSMS.Controls.Add(this.rbDeleteAllSMS);
            this.gboDeleteSMS.Location = new System.Drawing.Point(23, 149);
            this.gboDeleteSMS.Name = "gboDeleteSMS";
            this.gboDeleteSMS.Size = new System.Drawing.Size(375, 122);
            this.gboDeleteSMS.TabIndex = 44;
            this.gboDeleteSMS.TabStop = false;
            this.gboDeleteSMS.Text = "Delete SMS";
            // 
            // rbDeleteReadSMS
            // 
            this.rbDeleteReadSMS.AutoSize = true;
            this.rbDeleteReadSMS.Checked = true;
            this.rbDeleteReadSMS.Location = new System.Drawing.Point(16, 58);
            this.rbDeleteReadSMS.Name = "rbDeleteReadSMS";
            this.rbDeleteReadSMS.Size = new System.Drawing.Size(114, 17);
            this.rbDeleteReadSMS.TabIndex = 45;
            this.rbDeleteReadSMS.TabStop = true;
            this.rbDeleteReadSMS.Text = "Delete Read SMS ";
            this.rbDeleteReadSMS.UseVisualStyleBackColor = true;
            // 
            // btnDeleteSMS
            // 
            this.btnDeleteSMS.Location = new System.Drawing.Point(149, 84);
            this.btnDeleteSMS.Name = "btnDeleteSMS";
            this.btnDeleteSMS.Size = new System.Drawing.Size(75, 25);
            this.btnDeleteSMS.TabIndex = 42;
            this.btnDeleteSMS.Text = "Delete";
            this.btnDeleteSMS.UseVisualStyleBackColor = true;
            this.btnDeleteSMS.Click += new System.EventHandler(this.btnDeleteSMS_Click);
            // 
            // rbDeleteAllSMS
            // 
            this.rbDeleteAllSMS.AutoSize = true;
            this.rbDeleteAllSMS.Location = new System.Drawing.Point(16, 26);
            this.rbDeleteAllSMS.Name = "rbDeleteAllSMS";
            this.rbDeleteAllSMS.Size = new System.Drawing.Size(96, 17);
            this.rbDeleteAllSMS.TabIndex = 43;
            this.rbDeleteAllSMS.Text = "Delete All SMS";
            this.rbDeleteAllSMS.UseVisualStyleBackColor = true;
            // 
            // tabGSMLog
            // 
            this.tabGSMLog.Controls.Add(this.txtModemlog);
            this.tabGSMLog.Location = new System.Drawing.Point(4, 22);
            this.tabGSMLog.Name = "tabGSMLog";
            this.tabGSMLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabGSMLog.Size = new System.Drawing.Size(747, 380);
            this.tabGSMLog.TabIndex = 4;
            this.tabGSMLog.Text = "GSM AT Log";
            this.tabGSMLog.UseVisualStyleBackColor = true;
            // 
            // txtModemlog
            // 
            this.txtModemlog.Location = new System.Drawing.Point(0, 6);
            this.txtModemlog.Multiline = true;
            this.txtModemlog.Name = "txtModemlog";
            this.txtModemlog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtModemlog.Size = new System.Drawing.Size(743, 373);
            this.txtModemlog.TabIndex = 0;
            // 
            // tabServerLog
            // 
            this.tabServerLog.Controls.Add(this.lstServerLog);
            this.tabServerLog.Location = new System.Drawing.Point(4, 22);
            this.tabServerLog.Name = "tabServerLog";
            this.tabServerLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabServerLog.Size = new System.Drawing.Size(747, 380);
            this.tabServerLog.TabIndex = 5;
            this.tabServerLog.Text = "Server Log";
            this.tabServerLog.UseVisualStyleBackColor = true;
            // 
            // lstServerLog
            // 
            this.lstServerLog.FormattingEnabled = true;
            this.lstServerLog.HorizontalScrollbar = true;
            this.lstServerLog.Location = new System.Drawing.Point(1, 6);
            this.lstServerLog.Name = "lstServerLog";
            this.lstServerLog.Size = new System.Drawing.Size(742, 368);
            this.lstServerLog.TabIndex = 0;
            // 
            // tabSMSLog
            // 
            this.tabSMSLog.Controls.Add(this.grdSMSLog);
            this.tabSMSLog.Location = new System.Drawing.Point(4, 22);
            this.tabSMSLog.Name = "tabSMSLog";
            this.tabSMSLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabSMSLog.Size = new System.Drawing.Size(747, 380);
            this.tabSMSLog.TabIndex = 6;
            this.tabSMSLog.Text = "SMS Log";
            this.tabSMSLog.UseVisualStyleBackColor = true;
            // 
            // grdSMSLog
            // 
            this.grdSMSLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSMSLog.Location = new System.Drawing.Point(0, 0);
            this.grdSMSLog.Name = "grdSMSLog";
            this.grdSMSLog.Size = new System.Drawing.Size(747, 379);
            this.grdSMSLog.TabIndex = 0;
            // 
            // tabUsers
            // 
            this.tabUsers.Controls.Add(this.managedGrid1);
            this.tabUsers.Location = new System.Drawing.Point(4, 22);
            this.tabUsers.Name = "tabUsers";
            this.tabUsers.Size = new System.Drawing.Size(747, 380);
            this.tabUsers.TabIndex = 7;
            this.tabUsers.Text = "Users";
            this.tabUsers.UseVisualStyleBackColor = true;
            // 
            // managedGrid1
            // 
            this.managedGrid1.Location = new System.Drawing.Point(2, 2);
            this.managedGrid1.Name = "managedGrid1";
            this.managedGrid1.Size = new System.Drawing.Size(746, 376);
            this.managedGrid1.TabIndex = 0;
            // 
            // tabGlobalAddressBook
            // 
            this.tabGlobalAddressBook.Controls.Add(this.manageableGrid1);
            this.tabGlobalAddressBook.Location = new System.Drawing.Point(4, 22);
            this.tabGlobalAddressBook.Name = "tabGlobalAddressBook";
            this.tabGlobalAddressBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabGlobalAddressBook.Size = new System.Drawing.Size(747, 380);
            this.tabGlobalAddressBook.TabIndex = 8;
            this.tabGlobalAddressBook.Text = "Global Address Book";
            this.tabGlobalAddressBook.UseVisualStyleBackColor = true;
            // 
            // manageableGrid1
            // 
            this.manageableGrid1.Location = new System.Drawing.Point(0, 0);
            this.manageableGrid1.Name = "manageableGrid1";
            this.manageableGrid1.Size = new System.Drawing.Size(747, 380);
            this.manageableGrid1.TabIndex = 0;
            // 
            // gboConnectionStatus
            // 
            this.gboConnectionStatus.BackColor = System.Drawing.Color.Transparent;
            this.gboConnectionStatus.Controls.Add(this.lblNumClients);
            this.gboConnectionStatus.Controls.Add(this.label11);
            this.gboConnectionStatus.Controls.Add(this.label23);
            this.gboConnectionStatus.Controls.Add(this.lblConnectionStatus);
            this.gboConnectionStatus.Controls.Add(this.btnDisconnect);
            this.gboConnectionStatus.Location = new System.Drawing.Point(32, 521);
            this.gboConnectionStatus.Name = "gboConnectionStatus";
            this.gboConnectionStatus.Size = new System.Drawing.Size(680, 53);
            this.gboConnectionStatus.TabIndex = 41;
            this.gboConnectionStatus.TabStop = false;
            this.gboConnectionStatus.Text = "Connection Status";
            // 
            // lblNumClients
            // 
            this.lblNumClients.AutoSize = true;
            this.lblNumClients.Location = new System.Drawing.Point(378, 32);
            this.lblNumClients.Name = "lblNumClients";
            this.lblNumClients.Size = new System.Drawing.Size(75, 13);
            this.lblNumClients.TabIndex = 39;
            this.lblNumClients.Text = "<Num Clients>";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(356, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(161, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "Relay Server Connection Status:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(165, 13);
            this.label23.TabIndex = 37;
            this.label23.Text = "GSM Modem Connection Status :";
            // 
            // lblConnectionStatus
            // 
            this.lblConnectionStatus.AutoSize = true;
            this.lblConnectionStatus.ForeColor = System.Drawing.Color.Red;
            this.lblConnectionStatus.Location = new System.Drawing.Point(25, 32);
            this.lblConnectionStatus.Name = "lblConnectionStatus";
            this.lblConnectionStatus.Size = new System.Drawing.Size(79, 13);
            this.lblConnectionStatus.TabIndex = 36;
            this.lblConnectionStatus.Text = "Not Connected";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Location = new System.Drawing.Point(204, 19);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 25);
            this.btnDisconnect.TabIndex = 4;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnPDUtest
            // 
            this.btnPDUtest.Location = new System.Drawing.Point(34, 593);
            this.btnPDUtest.Name = "btnPDUtest";
            this.btnPDUtest.Size = new System.Drawing.Size(66, 28);
            this.btnPDUtest.TabIndex = 42;
            this.btnPDUtest.Text = "PDU test";
            this.btnPDUtest.UseVisualStyleBackColor = true;
            this.btnPDUtest.Click += new System.EventHandler(this.btnPDUtest_Click);
            // 
            // txtPDUOutput
            // 
            this.txtPDUOutput.Location = new System.Drawing.Point(89, 627);
            this.txtPDUOutput.Name = "txtPDUOutput";
            this.txtPDUOutput.Size = new System.Drawing.Size(332, 20);
            this.txtPDUOutput.TabIndex = 43;
            this.txtPDUOutput.TextChanged += new System.EventHandler(this.txtPDUOutput_TextChanged);
            // 
            // lblPDUOutput
            // 
            this.lblPDUOutput.AutoSize = true;
            this.lblPDUOutput.Location = new System.Drawing.Point(15, 630);
            this.lblPDUOutput.Name = "lblPDUOutput";
            this.lblPDUOutput.Size = new System.Drawing.Size(68, 13);
            this.lblPDUOutput.TabIndex = 44;
            this.lblPDUOutput.Text = "PDU Output:";
            // 
            // lblLengthTitle
            // 
            this.lblLengthTitle.AutoSize = true;
            this.lblLengthTitle.Location = new System.Drawing.Point(86, 660);
            this.lblLengthTitle.Name = "lblLengthTitle";
            this.lblLengthTitle.Size = new System.Drawing.Size(46, 13);
            this.lblLengthTitle.TabIndex = 45;
            this.lblLengthTitle.Text = "Length: ";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(138, 660);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(0, 13);
            this.lblLength.TabIndex = 46;
            // 
            // btnEncode7bit
            // 
            this.btnEncode7bit.Location = new System.Drawing.Point(141, 593);
            this.btnEncode7bit.Name = "btnEncode7bit";
            this.btnEncode7bit.Size = new System.Drawing.Size(104, 28);
            this.btnEncode7bit.TabIndex = 47;
            this.btnEncode7bit.Text = "Encode 7 bit";
            this.btnEncode7bit.UseVisualStyleBackColor = true;
            this.btnEncode7bit.Click += new System.EventHandler(this.btnEncode7bit_Click);
            // 
            // btnListen
            // 
            this.btnListen.Location = new System.Drawing.Point(625, 593);
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new System.Drawing.Size(90, 25);
            this.btnListen.TabIndex = 48;
            this.btnListen.Text = "Listen";
            this.btnListen.UseVisualStyleBackColor = true;
            this.btnListen.Click += new System.EventHandler(this.btnListen_Click);
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(526, 593);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(76, 25);
            this.btnSend.TabIndex = 49;
            this.btnSend.Text = "Send Data";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnSerialise
            // 
            this.btnSerialise.Location = new System.Drawing.Point(526, 630);
            this.btnSerialise.Name = "btnSerialise";
            this.btnSerialise.Size = new System.Drawing.Size(76, 25);
            this.btnSerialise.TabIndex = 50;
            this.btnSerialise.Text = "Serialise";
            this.btnSerialise.UseVisualStyleBackColor = true;
            this.btnSerialise.Click += new System.EventHandler(this.btnSerialise_Click);
            // 
            // tmrSocketCleanup
            // 
            this.tmrSocketCleanup.Enabled = true;
            this.tmrSocketCleanup.Interval = 1000;
            this.tmrSocketCleanup.Tick += new System.EventHandler(this.tmrSocketCleanup_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CedeSMSServer.Properties.Resources.TopBanner;
            this.pictureBox2.Location = new System.Drawing.Point(0, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(782, 73);
            this.pictureBox2.TabIndex = 51;
            this.pictureBox2.TabStop = false;
            // 
            // SMSapplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 583);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnSerialise);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnListen);
            this.Controls.Add(this.btnEncode7bit);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblLengthTitle);
            this.Controls.Add(this.lblPDUOutput);
            this.Controls.Add(this.txtPDUOutput);
            this.Controls.Add(this.btnPDUtest);
            this.Controls.Add(this.gboConnectionStatus);
            this.Controls.Add(this.tabSMSapplication);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SMSapplication";
            this.Text = "CedeSMS Relay Server";
            this.Load += new System.EventHandler(this.SMSapplication_Load);
            this.tabSMSapplication.ResumeLayout(false);
            this.tbPortSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gboPortSettings.ResumeLayout(false);
            this.gboPortSettings.PerformLayout();
            this.tbSendSMS.ResumeLayout(false);
            this.gboSendSMS.ResumeLayout(false);
            this.gboSendSMS.PerformLayout();
            this.tbReadSMS.ResumeLayout(false);
            this.gboReadSMS.ResumeLayout(false);
            this.tbDeleteSMS.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gboDeleteSMS.ResumeLayout(false);
            this.gboDeleteSMS.PerformLayout();
            this.tabGSMLog.ResumeLayout(false);
            this.tabGSMLog.PerformLayout();
            this.tabServerLog.ResumeLayout(false);
            this.tabSMSLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSMSLog)).EndInit();
            this.tabUsers.ResumeLayout(false);
            this.tabGlobalAddressBook.ResumeLayout(false);
            this.gboConnectionStatus.ResumeLayout(false);
            this.gboConnectionStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabSMSapplication;
        private System.Windows.Forms.TabPage tbPortSettings;
        private System.Windows.Forms.TabPage tbSendSMS;
        private System.Windows.Forms.TabPage tbReadSMS;
        private System.Windows.Forms.TabPage tbDeleteSMS;
        private System.Windows.Forms.GroupBox gboPortSettings;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWriteTimeOut;
        private System.Windows.Forms.TextBox txtReadTimeOut;
        private System.Windows.Forms.ComboBox cboParityBits;
        private System.Windows.Forms.ComboBox cboStopBits;
        private System.Windows.Forms.ComboBox cboDataBits;
        private System.Windows.Forms.ComboBox cboBaudRate;
        private System.Windows.Forms.ComboBox cboPortName;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox gboSendSMS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSIM;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button btnSendSMS;
        private System.Windows.Forms.GroupBox gboReadSMS;
        private System.Windows.Forms.Button btnReadSMS;
        private System.Windows.Forms.ListView lvwMessages;
        private System.Windows.Forms.ColumnHeader colSender;
        private System.Windows.Forms.ColumnHeader colMessage;
        private System.Windows.Forms.GroupBox gboConnectionStatus;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblConnectionStatus;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnDeleteSMS;
        private System.Windows.Forms.GroupBox gboDeleteSMS;
        private System.Windows.Forms.RadioButton rbDeleteAllSMS;
        private System.Windows.Forms.RadioButton rbDeleteReadSMS;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCountSMS;
        private System.Windows.Forms.TextBox txtCountSMS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ColumnHeader colIndex;
        private System.Windows.Forms.Button btnPDUtest;
        private System.Windows.Forms.TextBox txtPDUOutput;
        private System.Windows.Forms.Label lblPDUOutput;
        private System.Windows.Forms.Label lblLengthTitle;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Button btnEncode7bit;
        private System.Windows.Forms.TabPage tabGSMLog;
        private System.Windows.Forms.TextBox txtModemlog;
        private System.Windows.Forms.Button btnListen;
        private System.Windows.Forms.TabPage tabServerLog;
        private System.Windows.Forms.ListBox lstServerLog;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnSerialise;
        private System.Windows.Forms.Label lblNumClients;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Timer tmrSocketCleanup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabSMSLog;
        private System.Windows.Forms.DataGridView grdSMSLog;
        private System.Windows.Forms.TabPage tabUsers;
        private ManageableGrid managedGrid1;
        private System.Windows.Forms.TabPage tabGlobalAddressBook;
        private ManageableGrid manageableGrid1;
        private System.Windows.Forms.CheckBox chkConnectOnStartup;
        
    }
}

