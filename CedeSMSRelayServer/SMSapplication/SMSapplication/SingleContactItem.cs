using System;
using System.Collections.Generic;
using System.Text;

namespace SMSapplication
{
    public class SingleContactItem
    {
        public String _Name;
        public String _MobileNumber;
        public String _GroupName;

        public String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        public String MobileNumber
        {
            get
            {
                return _MobileNumber;
            }
            set
            {
                _MobileNumber = value;
            }
        }

        public String GroupName
        {
            get
            {
                return _GroupName;
            }
            set
            {
                _GroupName = value;
            }
        }

        public SingleContactItem(String name, String mobilenumber, String groupname)
        {
            _Name = name;
            _MobileNumber = mobilenumber;
            _GroupName = groupname;
        }

        public SingleContactItem()
        {

        }
    }
}
