using System;
using System.Collections.Generic;
using System.Text;

namespace SMSapplication
{
    public class SingleSMSLog
    {
        public DateTime DateTimeSent;
        public String Username;
        public bool Encrypted;
        public String MobileNumber;
        public String Message;
        public bool Success;

        public SingleSMSLog(DateTime datetimesent, String username, bool encrypted, String mobilenumber, String message, bool success)
        {
            DateTimeSent = datetimesent;
            Username = username;
            Encrypted = encrypted;
            MobileNumber = mobilenumber;
            Message = message;
            Success = success;
        }

        public SingleSMSLog()
        {
        }
    }
}
