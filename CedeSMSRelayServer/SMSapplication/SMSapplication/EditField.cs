using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SMSapplication
{
    public partial class EditField : UserControl
    {
        public String FieldName
        {
            get
            {
                return lblName.Text;
            }
            set
            {
                lblName.Text = value;
            }
        }

        public String FieldValue
        {
            get
            {
                return txtField.Text;
            }
            set
            {
                txtField.Text = value;
            }
        }

        public EditField()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
