using System;
using System.Collections.Generic;
using System.Text;

namespace SMSapplication
{
    public class AppSettings
    {
        public String PortName = "COM1";
        public String BaudRate = "9600";
        public String DataBits = "8";
        public String StopBits = "1";
        public String ParityBits = "None";
        public String ReadTimeout = "300";
        public String WriteTimeout = "300";
        public bool ConnectOnStartup = false;

        public AppSettings()
        {
        }
    }
}
