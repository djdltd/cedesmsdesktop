/*
CedeSMS GSM Relay Server
 * CedeSoft Ltd (c) 2009
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;
using System.Reflection;
using SMSapplication.Classes;
using SMSPDULib;

namespace SMSapplication
{
    public partial class SMSapplication : Form
    {

        #region Constructor
        public SMSapplication()
        {
            InitializeComponent();
        }
        #endregion

        #region Private Variables
        SerialPort port = new SerialPort();
        clsSMS objclsSMS = new clsSMS();
        ShortMessageCollection objShortMessageCollection = new ShortMessageCollection();
        #endregion

        const int MAX_CLIENTS = 10;

        public AsyncCallback pfnWorkerCallBack;
        private Socket m_mainSocket;
        private Socket[] m_workerSocket = new Socket[MAX_CLIENTS+1];
        private bool[] m_socketbusy = new bool[MAX_CLIENTS+1];

        private int m_actualclientCount = 0;

        public delegate void AnalyseDataCallback(String strData);

        public event AnalyseDataCallback AnalyseEvent;

        private ArrayList m_authenticatedusers = new ArrayList();

        public delegate void ReceiveLogEvent(string MessageLog);

        public event ReceiveLogEvent OnReceiveLogEvent;


        private List<SingleSMSLog> m_smslog = new List<SingleSMSLog>();
        private List<SingleContactItem> m_globaladdressbook = new List<SingleContactItem>();
        AppSettings m_appsettings = new AppSettings();

        #region Private Methods
       
        #endregion

        #region Private Events

        private bool IsAuthUserOk(String strUsername, String strPassword)
        {
            AuthUser curUser = new AuthUser();
            for (int a = 0; a < m_authenticatedusers.Count; a++)
            {
                curUser = (AuthUser) m_authenticatedusers[a];

                if (curUser.Username.ToUpper() == strUsername.ToUpper())
                {
                    if (curUser.Password == strPassword)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        ArrayList ToUserArrayList(List<AuthUser> list)
        {
            ArrayList newlist = new ArrayList();
            for (int o = 0; o < list.Count; o++)
            {
                AuthUser item = new AuthUser();
                item = (AuthUser)list[o];
                newlist.Add(item);
            }
            return newlist;
        }

        List<AuthUser> FromUserArrayList(ArrayList list)
        {
            List<AuthUser> newlist = new List<AuthUser>();
            for (int o = 0; o < list.Count; o++)
            {
                AuthUser item = new AuthUser();
                item = (AuthUser)list[o];
                newlist.Add(item);
            }
            return newlist;
        }

        ArrayList ToContactArrayList(List<SingleContactItem> list)
        {
            ArrayList newlist = new ArrayList();
            for (int o = 0; o < list.Count; o++)
            {
                SingleContactItem item = new SingleContactItem();
                item = (SingleContactItem)list[o];
                newlist.Add(item);
            }
            return newlist;
        }

        List<SingleContactItem> FromContactArrayList(ArrayList list)
        {
            List<SingleContactItem> newlist = new List<SingleContactItem> ();
            for (int o = 0; o < list.Count; o++)
            {
                SingleContactItem item = new SingleContactItem();
                item = (SingleContactItem)list[o];
                newlist.Add(item);
            }
            return newlist;
        }

        private String GetAppSettingsDir()
        {
            // Function to retrieve the applications settings directory which
            // is usually APPDATA\\FileCopyProgress
            String strSavepath = System.Environment.GetEnvironmentVariable("APPDATA") + "\\CedeSMSServer";
            Directory.CreateDirectory(strSavepath);
            return strSavepath + "\\";
        }

        public void LoadXMLAuthUsers ()
        {
            if (File.Exists(GetAppSettingsDir() + "AuthUsers.xml") == true)
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(List<AuthUser>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(GetAppSettingsDir() + "AuthUsers.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                List<AuthUser> authuserlist = new List<AuthUser>();
                authuserlist = (List<AuthUser>)mySerializer.Deserialize(myFileStream);

                // Convert to array list
                m_authenticatedusers = ToUserArrayList(authuserlist);

                myFileStream.Close();
            }
        }

        public void SaveXMLAuthUsers()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof (List<AuthUser>));
            StreamWriter myWriter = new StreamWriter(GetAppSettingsDir() + "AuthUsers.xml");

            mySerializer.Serialize(myWriter, FromUserArrayList(m_authenticatedusers));
            myWriter.Close();
        }

        public void SaveXMLGlobalContacts()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof (List<SingleContactItem>));
            StreamWriter myWriter = new StreamWriter(GetAppSettingsDir() + "GlobalContacts.xml");

            mySerializer.Serialize(myWriter, m_globaladdressbook);
            myWriter.Close();
        }

        public void LoadXMLGlobalContacts()
        {
            if (File.Exists(GetAppSettingsDir() + "GlobalContacts.xml") == true)
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof (List<SingleContactItem>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(GetAppSettingsDir() + "GlobalContacts.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.                
                m_globaladdressbook = (List<SingleContactItem>)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        public void SaveXMLSMSLog()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(List<SingleSMSLog>));
            StreamWriter myWriter = new StreamWriter(GetAppSettingsDir() + "SMSLog.xml");

            
            mySerializer.Serialize(myWriter, m_smslog);
            myWriter.Close();
        }

        public void LoadXMLSMSLog()
        {
            if (File.Exists(GetAppSettingsDir() + "SMSLog.xml") == true)
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(List<SingleSMSLog>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(GetAppSettingsDir() + "SMSLog.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.                
                m_smslog = (List<SingleSMSLog>)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        public void LoadXMLAppSettings()
        {
            if (File.Exists(GetAppSettingsDir() + "Settings.xml") == true)
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(AppSettings));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(GetAppSettingsDir() + "Settings.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                m_appsettings = (AppSettings)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        public void SaveXMLAppSettings()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(AppSettings));
            StreamWriter myWriter = new StreamWriter(GetAppSettingsDir() + "Settings.xml");

            mySerializer.Serialize(myWriter, m_appsettings);
            myWriter.Close();
        }

        private void AppSettingsToForm()
        {
            cboPortName.Text = m_appsettings.PortName;
            cboBaudRate.Text = m_appsettings.BaudRate;
            cboDataBits.Text = m_appsettings.DataBits;
            cboStopBits.Text = m_appsettings.StopBits;
            cboParityBits.Text = m_appsettings.ParityBits;
            txtReadTimeOut.Text = m_appsettings.ReadTimeout;
            txtWriteTimeOut.Text = m_appsettings.WriteTimeout;
            chkConnectOnStartup.Checked = m_appsettings.ConnectOnStartup;
        }


        private void SMSapplication_Load(object sender, EventArgs e)
        {
            //// Add the authenticated users to the list
            //m_authenticatedusers.Add(new AuthUser("draperd", "harpenden"));
            //m_authenticatedusers.Add(new AuthUser("espieg", "harpenden"));
            //m_authenticatedusers.Add(new AuthUser("pouncei", "harpenden"));
            //m_authenticatedusers.Add(new AuthUser("rickardp", "harpenden"));
            //m_authenticatedusers.Add(new AuthUser("cariono", "harpenden"));

            //// Add the global address book contacts
            //m_globaladdressbook.Add(new SingleContactItem("Danny's Blackberry", "+447710978853", "Global"));
            //m_globaladdressbook.Add(new SingleContactItem("Danny's iPhone", "+447540770830", "Global"));
            //m_globaladdressbook.Add(new SingleContactItem("Glenn Espie", "+447810882897", "Global"));
            //m_globaladdressbook.Add(new SingleContactItem("Pari Rickard", "+447976225277", "Global"));
            //m_globaladdressbook.Add(new SingleContactItem("Ian Pounce", "+447833593910", "Global"));
            //m_globaladdressbook.Add(new SingleContactItem("Olivier Carion", "+447940728031", "Global"));



            LoadXMLSMSLog();
            LoadXMLAuthUsers();
            LoadXMLGlobalContacts();

            managedGrid1.Populate(m_authenticatedusers, typeof (AuthUser), new AuthUser());
            manageableGrid1.Populate(ToContactArrayList(m_globaladdressbook), typeof(SingleContactItem), new SingleContactItem());

            managedGrid1.OnListChanged += new ManageableGrid.ListChanged(managedGrid1_OnListChanged);
            manageableGrid1.OnListChanged += new ManageableGrid.ListChanged(manageableGrid1_OnListChanged);

            try
            {
                #region Display all available COM Ports
                string[] ports = SerialPort.GetPortNames();

                // Add all port names to the combo box:
                foreach (string port in ports)
                {
                    this.cboPortName.Items.Add(port);
                }
                #endregion

                //Remove tab pages
                this.tabSMSapplication.TabPages.Remove(tbSendSMS);
                this.tabSMSapplication.TabPages.Remove(tbReadSMS);
                this.tabSMSapplication.TabPages.Remove(tbDeleteSMS);

                this.btnDisconnect.Enabled = false;


                // Event handlers
                objclsSMS.OnModemLogEvent += new clsSMS.ModemLogEvent(objclsSMS_OnModemLogEvent);
            }
            catch(Exception ex)
            {
                ErrorLog(ex.Message);
            }

            LoadXMLAppSettings();
            AppSettingsToForm();
            RefreshSMSLog();

            OnReceiveLogEvent += new ReceiveLogEvent(SMSapplication_OnReceiveLogEvent);
            AnalyseEvent += new AnalyseDataCallback(SMSapplication_AnalyseEvent);


            // Start the relay server
            StartServer();
            //StartTCPServer();

            if (m_appsettings.ConnectOnStartup == true)
            {
                btnOK_Click(sender, e);
            }
        }

        void manageableGrid1_OnListChanged()
        {
            //throw new Exception("The method or operation is not implemented.");
            SaveXMLGlobalContacts();
        }

        void managedGrid1_OnListChanged()
        {
            SaveXMLAuthUsers();
            //throw new Exception("The method or operation is not implemented.");
        }



        void SMSapplication_AnalyseEvent(string strData)
        {
            OnReceiveLogEvent(strData);
            AnalyseMessage(strData);
            //throw new Exception("The method or operation is not implemented.");
        }

        private void StartTCPServer()
        {
            TcpListener server = new TcpListener(4047);

            server.Start();
            server.BeginAcceptTcpClient(new AsyncCallback(OnTCPClientConnect), server);

            lstServerLog.Items.Add("TCP Relay Server started.");

            //m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
        }

        public void OnTCPClientConnect(IAsyncResult asyn)
        {
            MessageBox.Show("client connected!");

            TcpListener listener = asyn.AsyncState as TcpListener;

            TcpClient client = listener.EndAcceptTcpClient(asyn);

            listener.BeginAcceptTcpClient(this.OnTCPClientConnect, listener);

            NetworkStream myStream = client.GetStream();

            //myStream.Begin
        }

        void SMSapplication_OnReceiveLogEvent(string MessageLog)
        {
            //lstServerLog.Items.Add(MessageLog);
        }

        void objclsSMS_OnModemLogEvent(string MessageLog, string command)
        {
            //throw new Exception("The method or operation is not implemented.");
            txtModemlog.Text += command + "\r\n";
            txtModemlog.Text += MessageLog.Replace ("\r\n", "") + "\r\n";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {

                //Open communication port 
                this.port = objclsSMS.OpenPort(this.cboPortName.Text, Convert.ToInt32(this.cboBaudRate.Text), Convert.ToInt32(this.cboDataBits.Text), Convert.ToInt32(this.txtReadTimeOut.Text), Convert.ToInt32(this.txtWriteTimeOut.Text));

                if (this.port != null)
                {
                    m_appsettings.PortName = cboPortName.Text;
                    m_appsettings.BaudRate = cboBaudRate.Text;
                    m_appsettings.DataBits = cboDataBits.Text;
                    m_appsettings.StopBits = cboStopBits.Text;
                    m_appsettings.ParityBits = cboParityBits.Text;
                    m_appsettings.ReadTimeout = txtReadTimeOut.Text;
                    m_appsettings.WriteTimeout = txtWriteTimeOut.Text;
                    m_appsettings.ConnectOnStartup = chkConnectOnStartup.Checked;

                    SaveXMLAppSettings();

                    //this.tabSMSapplication.TabPages.Remove(tbPortSettings);
                    this.gboPortSettings.Enabled = false;

                    //MessageBox.Show("Modem is connected at PORT " + this.cboPortName.Text);

                    //Add tab pages
                    this.tabSMSapplication.TabPages.Add(tbSendSMS);
                    //this.tabSMSapplication.TabPages.Add(tbReadSMS);
                    //this.tabSMSapplication.TabPages.Add(tbDeleteSMS);

                    this.lblConnectionStatus.Text = "Connected at " + this.cboPortName.Text;
                    this.lblConnectionStatus.ForeColor = Color.Green;
                    this.btnDisconnect.Enabled = true;

                    objclsSMS.sendInitMessages(this.port);
                }

                else
                {
                    MessageBox.Show("Invalid port settings");
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }

        }
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                this.gboPortSettings.Enabled = true;
                objclsSMS.ClosePort(this.port);

                //Remove tab pages
                this.tabSMSapplication.TabPages.Remove(tbSendSMS);
                this.tabSMSapplication.TabPages.Remove(tbReadSMS);
                this.tabSMSapplication.TabPages.Remove(tbDeleteSMS);

                this.lblConnectionStatus.ForeColor = Color.Red;
                this.lblConnectionStatus.Text = "Not Connected";
                this.btnDisconnect.Enabled = false;

            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }

        private void btnSendSMS_Click(object sender, EventArgs e)
        {

            //.............................................. Send SMS ....................................................
            try
            {

                if (objclsSMS.sendMsg(this.port, this.txtSIM.Text, this.txtMessage.Text, false))
                {
                    MessageBox.Show("Message has sent successfully");
                }
                else
                {
                    MessageBox.Show("Failed to send message");
                }
                
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }
        private void btnReadSMS_Click(object sender, EventArgs e)
        {
            try
            {
                //count SMS 
                int uCountSMS = objclsSMS.CountSMSmessages(this.port);
                if (uCountSMS > 0)
                {
                    // If SMS exist then read SMS
                    #region Read SMS
                    //.............................................. Read all SMS ....................................................
                    objShortMessageCollection = objclsSMS.ReadSMS(this.port);
                    foreach (ShortMessage msg in objShortMessageCollection)
                    {

                        ListViewItem item = new ListViewItem(new string[] { msg.Index ,msg.Sender, msg.Message });
                        item.Tag = msg;
                        lvwMessages.Items.Add(item);

                    }
                    #endregion
                }
                else
                {
                    lvwMessages.Clear();
                    MessageBox.Show("There is no message in SIM");

                    
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }
        private void btnDeleteSMS_Click(object sender, EventArgs e)
        {
            try
            {
                //Count SMS 
                int uCountSMS = objclsSMS.CountSMSmessages(this.port);
                if (uCountSMS > 0)
                {
                    DialogResult dr = MessageBox.Show("Are u sure u want to delete the SMS?", "Delete confirmation", MessageBoxButtons.YesNo);

                    if (dr.ToString() == "Yes")
                    {
                        #region Delete SMS

                        if (this.rbDeleteAllSMS.Checked)
                        {                           
                            //...............................................Delete all SMS ....................................................

                            #region Delete all SMS
                            string strCommand = "AT+CMGD=1,4";
                            if (objclsSMS.DeleteMsg(this.port, strCommand))
                            {
                                MessageBox.Show("Messages has deleted successfuly ");
                            }
                            else
                            {
                                MessageBox.Show("Failed to delete messages ");
                            }
                            #endregion
                            
                        }
                        else if (this.rbDeleteReadSMS.Checked)
                        {                          
                            //...............................................Delete Read SMS ....................................................

                            #region Delete Read SMS
                            string strCommand = "AT+CMGD=1,3";
                            if (objclsSMS.DeleteMsg(this.port, strCommand))
                            {
                                MessageBox.Show("Messages has deleted successfuly ");
                            }
                            else
                            {
                                MessageBox.Show("Failed to delete messages ");
                            }
                            #endregion

                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }

        }
        private void btnCountSMS_Click(object sender, EventArgs e)
        {
            try
            {
                //Count SMS
                int uCountSMS = objclsSMS.CountSMSmessages(this.port);
                this.txtCountSMS.Text = uCountSMS.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }

        #endregion

        #region Error Log
        public void ErrorLog(string Message)
        {
            StreamWriter sw = null;

            try
            {
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                string sPathName = @"E:\";

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                sw = new StreamWriter(sPathName + "SMSapplication_ErrorLog_" + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + Message);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog(ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }


        }
        #endregion 



        private void btnPDUtest_Click(object sender, EventArgs e)
        {
            SMS sms = new SMS();
            sms.Direction = SMSDirection.Submited;            
            sms.PhoneNumber = "+447540770830";
            //sms.ValidityPeriod = new TimeSpan(4, 0, 0, 0);
            sms.Message = "Hello, this is so very cool indeed!!!";
            String pduSource = sms.Compose(SMS.SMSEncoding._7bit);
                        
            txtPDUOutput.Text = pduSource;
            lblLength.Text = "Octet Length: " + sms.OctetLength;

            //SMS smsdecode = new SMS();
            //SMS.Fetch(smsdecode, ref pduSource);

            //lblLength.Text = Convert.ToString((pduSource.Length - 2) / 2);

            //MessageBox.Show(smsdecode.Message);

            //char c = (char)65;
            
            //MessageBox.Show(SMS.BitsToHex (SMS.CharTo7Bits(c)));

            //MessageBox.Show(SMS.Encode7bit("It is easy to send text messages."));


            //txtPDUOutput.Text = SMS.Encode7Bit("Hello, I think that is is really cool!!!");

            //int septets = ((txtPDUOutput.Text.Length / 2) * 8) / 7;

            //MessageBox.Show(septets.ToString() + " septets");

            //txtPDUOutput.Text = SMS.Encode7Bit("abc");

            //MessageBox.Show(SMS.BitsToHex("00000111"));

            //Encoding.UTF7.GetString (
        }

        private void txtPDUOutput_TextChanged(object sender, EventArgs e)
        {
            //lblLength.Text = txtPDUOutput.Text.Length.ToString();
        }

        private void btnEncode7bit_Click(object sender, EventArgs e)
        {
            //String strtest = "a";
            
        }

        private void btnListen_Click(object sender, EventArgs e)
        {            


        }


        private void StartServer()
        {
            try
            {
                m_mainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, 4047);

                m_mainSocket.Bind(ipLocal);

                m_mainSocket.Listen(4);
                                
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);

                lstServerLog.Items.Add("SMS Relay Server started successfully, running on port " + ipLocal.Port.ToString ());
            }
            catch (Exception e)
            {
                lstServerLog.Items.Add("Error Starting SMS Relay Server: " + e.Message);
            }                       
        }

        public int GetNextFreeSocket()
        {
            for (int a = 0; a < MAX_CLIENTS+1; a++)
            {
                if (m_socketbusy[a] == false)
                {
                    return a;
                }
            }

            return -1;
        }

        // This is the call back function, which will be invoked when a client is connected
        public void OnClientConnect(IAsyncResult asyn)
        {
            try
            {
                // Here we complete/end the BeginAccept() asynchronous call
                // by calling EndAccept() - which returns the reference to
                // a new Socket object
                int availsocket = GetNextFreeSocket();

                if (availsocket != -1)
                {
                    m_workerSocket[availsocket] = m_mainSocket.EndAccept(asyn);
                    // Let the worker Socket do the further processing for the 
                    // just connected client
                    WaitForData(m_workerSocket[availsocket]);
                    // Now increment the client count

                    // Display this client connection as a status message on the GUI	
                    //String str = String.Format("Client # {0} connected", m_clientCount);

                    CommPacket packet = new CommPacket();

                    if (m_actualclientCount == MAX_CLIENTS)
                    {                        
                        packet.MessageType = "SERVERFULL";
                        packet.ClientNumber = availsocket;
                    }
                    else
                    {                        
                        packet.MessageType = "NEEDAUTHENTICATION";
                        packet.ClientNumber = availsocket;
                    }                    

                    // Send authentication request to the client
                    SendToClient(availsocket, packet);

                    //++m_clientCount;

                    //textBoxMsg.Text = str;
                    //MessageBox.Show(str);
                }
                
                // Since the main Socket is now free, it can go back and wait for
                // other clients who are attempting to connect
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\n OnClientConnection: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                //MessageBox.Show(se.Message);
            }



        }


        public class SocketPacket
        {
            public System.Net.Sockets.Socket m_currentSocket;
            public byte[] dataBuffer = new byte[1000000];
        }

        // Start waiting for data from the client
        public void WaitForData(System.Net.Sockets.Socket soc)
        {
            try
            {
                if (pfnWorkerCallBack == null)
                {
                    // Specify the call back function which is to be 
                    // invoked when there is any write activity by the 
                    // connected client
                    pfnWorkerCallBack = new AsyncCallback(OnDataReceived);
                }
                SocketPacket theSocPkt = new SocketPacket();
                theSocPkt.m_currentSocket = soc;
                // Start receiving any data written by the connected client
                // asynchronously
                soc.BeginReceive(theSocPkt.dataBuffer, 0,
                                   theSocPkt.dataBuffer.Length,
                                   SocketFlags.None,
                                   pfnWorkerCallBack,
                                   theSocPkt);
                
            }
            catch (SocketException se)
            {
                //MessageBox.Show(se.Message);
            }
        }

        public void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                SocketPacket socketData = (SocketPacket)asyn.AsyncState;

                int iRx = 0;
                // Complete the BeginReceive() asynchronous call by EndReceive() method
                // which will return the number of characters written to the stream 
                // by the client
                iRx = socketData.m_currentSocket.EndReceive(asyn);
                char[] chars = new char[iRx + 1];
                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                int charLen = d.GetChars(socketData.dataBuffer,
                                         0, iRx, chars, 0);
                System.String szData = new System.String(chars);
                //richTextBoxReceivedMsg.AppendText(szData);

                //lstServerLog.Items.Add(szData);

                if (szData.Contains("xml version") == true)
                {
                    this.Invoke(AnalyseEvent, szData);
                    //OnReceiveLogEvent(szData);
                    //AnalyseMessage(szData);
                }

                // Continue the waiting for data on the Socket
                WaitForData(socketData.m_currentSocket);
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                //MessageBox.Show(se.Message);
            }
        }

        private void AddToServerLog(String Message)
        {
            lstServerLog.Items.Add(DateTime.Now.ToString() + ": " + Message);
        }

        private void AnalyseMessage(String strXml)
        {
            CommPacket rcvPacket = new CommPacket();

            rcvPacket = (CommPacket)FromXml(strXml, typeof(CommPacket));

            int fromclient = rcvPacket.ClientNumber;

            if (rcvPacket.MessageType == "AUTHENTICATION")
            {
                AddToServerLog("Client connected, User: " + rcvPacket.Username + ", Client: " + fromclient.ToString ());
                //MessageBox.Show("need to send auth.");
                CommPacket sendpacket = new CommPacket();
                
                if (IsAuthUserOk (rcvPacket.Username, rcvPacket.Password) == true)
                {
                    AddToServerLog("Client password ok.");
                    sendpacket.MessageType = "AUTHENTICATIONOK";

                    if (m_globaladdressbook.Count > 0)
                    {
                        sendpacket.GALPresent = true;
                        sendpacket.GALData = ToXml(m_globaladdressbook, typeof(List<SingleContactItem>));
                    }

                }
                else
                {
                    AddToServerLog("Client password invalid.");
                    sendpacket.MessageType = "AUTHENTICATIONDENIED";
                }

                SendToClient(fromclient, sendpacket);                
            }

            if (rcvPacket.MessageType == "SENDSMS")
            {
                //MessageBox.Show("SendSMS received!");
                CommPacket sendpacket = new CommPacket();

                SingleSMSLog smslogitem = new SingleSMSLog();
                smslogitem.DateTimeSent = DateTime.Now;
                smslogitem.Encrypted = rcvPacket.UseEncryption;
                smslogitem.Message = rcvPacket.Message;
                smslogitem.Username = rcvPacket.Username;
                smslogitem.MobileNumber = rcvPacket.PhoneNumber;

                if (rcvPacket.UseEncryption == true)
                {
                    CipherLite enc = new CipherLite();
                    String strPassword = rcvPacket.Password;

                    if (strPassword != null)
                    {

                        String EncryptedMessage = enc.EncryptTextEx(strPassword, rcvPacket.Message);

                        try
                        {


                            if (objclsSMS.sendMsg(this.port, rcvPacket.PhoneNumber, EncryptedMessage, rcvPacket.UseEncryption))
                            {
                                //MessageBox.Show("Message has sent successfully");
                                sendpacket.MessageType = "SENTSMSOK";
                                smslogitem.Success = true;                                
                            }
                            else
                            {
                                //MessageBox.Show("Failed to send message");
                                sendpacket.MessageType = "SENTSMSFAILED";
                                smslogitem.Success = false;
                            }

                            SendToClient(fromclient, sendpacket);
                        }
                        catch (Exception ex)
                        {
                            ErrorLog(ex.Message);
                            sendpacket.MessageType = "SENTSMSFAILED";
                            smslogitem.Success = false;
                            SendToClient(fromclient, sendpacket);
                        }
                    }
                    else
                    {
                        sendpacket.MessageType = "SENTSMSFAILED";
                        smslogitem.Success = false;
                        SendToClient(fromclient, sendpacket);
                    }
                }
                else
                {
                    try
                    {


                        if (objclsSMS.sendMsg(this.port, rcvPacket.PhoneNumber, rcvPacket.Message, rcvPacket.UseEncryption))
                        {
                            //MessageBox.Show("Message has sent successfully");
                            sendpacket.MessageType = "SENTSMSOK";
                            smslogitem.Success = true;
                        }
                        else
                        {
                            //MessageBox.Show("Failed to send message");
                            sendpacket.MessageType = "SENTSMSFAILED";
                            smslogitem.Success = false;
                        }

                        SendToClient(fromclient, sendpacket);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog(ex.Message);
                        sendpacket.MessageType = "SENTSMSFAILED";
                        smslogitem.Success = false;
                        SendToClient(fromclient, sendpacket);
                    }
                }

                AddSMSLogItem(smslogitem);                 
            }
        }


        private void AddSMSLogItem(SingleSMSLog logitem)
        {
            m_smslog.Add(logitem);
            RefreshSMSLog();
            SaveXMLSMSLog();
        }

        private void RefreshSMSLog()
        {
            grdSMSLog.Columns.Clear();

            grdSMSLog.Columns.Add("Date/Time", "Date/Time");
            grdSMSLog.Columns.Add("Encrypted", "Encrypted");
            grdSMSLog.Columns.Add("Username", "Username");
            grdSMSLog.Columns.Add("Mobile Number", "Mobile Number");
            grdSMSLog.Columns.Add("Message", "Message");
            grdSMSLog.Columns.Add("Success", "Success");

            for (int c = 0; c < m_smslog.Count; c++)
            {
                SingleSMSLog currentlog = new SingleSMSLog();
                currentlog = m_smslog[c];

                grdSMSLog.Rows.Add();

                grdSMSLog.Rows[c].Cells[0].Value = currentlog.DateTimeSent.ToString();
                grdSMSLog.Rows[c].Cells[1].Value = currentlog.Encrypted.ToString();
                grdSMSLog.Rows[c].Cells[2].Value = currentlog.Username;
                grdSMSLog.Rows[c].Cells[3].Value = currentlog.MobileNumber;
                grdSMSLog.Rows[c].Cells[4].Value = currentlog.Message;
                grdSMSLog.Rows[c].Cells[5].Value = currentlog.Success.ToString();                
            }
        }

        private void RefreshUsers()
        {
            //int p = 0;
            //Type t = typeof(AuthUser);
            //PropertyInfo[] pinfo = t.GetProperties();

            //for (p = 0; p < pinfo.Length; p++)
            //{
            //    PropertyInfo pi = pinfo[p];
            //    grdUsers.Columns.Add(pi.Name, pi.Name);
            //}

            //for (int u = 0; u < m_authenticatedusers.Count; u++)
            //{
            //    AuthUser cuser = new AuthUser();
            //    cuser = (AuthUser) m_authenticatedusers[u];

            //    grdUsers.Rows.Add ();

            //    for (p = 0; p < pinfo.Length; p++)
            //    {
            //        PropertyInfo pi = pinfo[p];                    
            //        Object obj = pi.GetValue(cuser, null);                    
            //        grdUsers.Rows[u].Cells[p].Value = obj.ToString();
            //    }                                                
            //}
        }

        private void SendToClient(int iclient, CommPacket packet)
        {
            try
            {
                Object objData = (String)ToXml(packet, typeof(CommPacket));
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(objData.ToString());

                if (m_workerSocket[iclient] != null)
                {
                    if (m_workerSocket[iclient].Connected)
                    {
                        m_workerSocket[iclient].Send(byData);
                    }
                }
            }
            catch (SocketException se)
            {
                //MessageBox.Show(se.Message);
            }
        }

        private bool PingClient(int iclient, CommPacket packet)
        {
            try
            {
                Object objData = (String)ToXml(packet, typeof(CommPacket));
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(objData.ToString());

                if (m_workerSocket[iclient] != null)
                {
                    if (m_workerSocket[iclient].Connected)
                    {
                        m_workerSocket[iclient].Send(byData);
                    }
                }
                return true;
            }
            catch (SocketException se)
            {
                //MessageBox.Show(se.Message);
                return false;
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {

        }


        public static string ToXml(object Obj, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType);
            MemoryStream memStream;
            memStream = new MemoryStream();
            XmlTextWriter xmlWriter;
            xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
            xmlWriter.Namespaces = true;
            ser.Serialize(xmlWriter, Obj);
            xmlWriter.Close();
            memStream.Close();
            string xml;
            xml = Encoding.UTF8.GetString(memStream.GetBuffer());
            xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
            xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1));
            return xml;

        }

        public static object FromXml(string Xml, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType);
            StringReader stringReader;
            stringReader = new StringReader(Xml);
            XmlTextReader xmlReader;
            xmlReader = new XmlTextReader(stringReader);
            object obj;
            obj = ser.Deserialize(xmlReader);
            xmlReader.Close();
            stringReader.Close();
            return obj;

        }

        private void btnSerialise_Click(object sender, EventArgs e)
        {
            CommPacket msg = new CommPacket();

            msg.MessageType = "HELLO";
            msg.UseEncryption = false;
            msg.Username = "djdltd";
            msg.Password = "password";
            msg.PhoneNumber = "+447540770830";
            msg.Message = "Hello this is a test";

            MessageBox.Show (ToXml(msg, typeof(CommPacket)));
        }

        private void tmrSocketCleanup_Tick(object sender, EventArgs e)
        {
            //private Socket[] m_workerSocket = new Socket[10];
            int clientcount = 0;

            for (int a = 0; a < m_workerSocket.Length; a++)
            {
                if (m_workerSocket[a] != null)
                {
                    CommPacket packet = new CommPacket();
                    packet.MessageType = "PING";                    

                    if (m_workerSocket[a].Connected == true)
                    {
                        if (PingClient(a, packet) == true)
                        {
                            m_socketbusy[a] = true;
                            clientcount++;
                        }
                        else
                        {                            
                            m_socketbusy[a] = false;
                        }
                    }
                    else
                    {                        
                        m_socketbusy[a] = false;
                    }
                }
                else
                {                    
                    m_socketbusy[a] = false;
                }
            }

            lblNumClients.Text = clientcount + " clients connected.";

            m_actualclientCount = clientcount;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //RefreshUsers();

            //NewItem newItem = new NewItem();

            //AuthUser myuser = new AuthUser();
            //myuser = (AuthUser) m_authenticatedusers[0];

            //newItem.Populate(myuser);

            //newItem.Show();

        }

        private void chkConnectOnStartup_CheckedChanged(object sender, EventArgs e)
        {
            m_appsettings.ConnectOnStartup = chkConnectOnStartup.Checked;
            SaveXMLAppSettings();
        }
    }

    public class AuthUser 
    {
        private String _Username;
        private String _Password;

        public String Username
        {
            get
            {
                return _Username;
            }
            set
            {
                _Username = value;
            }
        }

        public String Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }

        public AuthUser()
        {
            
        }

        public AuthUser(String username, String password)
        {
            _Username = username;
            _Password = password;
        }
    }
}