using System;
using System.Collections.Generic;
using System.Text;

namespace SMSapplication
{
    public class CommPacket
    {
        public String MessageType;
        public int ClientNumber;
        public String Username;
        public String Password;
        public String PhoneNumber;
        public String Message;
        public bool UseEncryption;
        public bool GALPresent;
        public String GALData;
    }
}
