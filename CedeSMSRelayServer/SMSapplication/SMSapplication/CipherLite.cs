using System;
using System.Collections.Generic;
using System.Text;

namespace SMSapplication
{
    public class CipherLite
    {
        private Random rnd = new Random();

        public long Raise(long lValue, long lPower)
        {
            long lRes = lValue;

            int l = 0;
            for (l = 1; l < lPower; l++)
            {
                lRes = lRes * lValue;
            }
            return lRes;
        }

        public String Expand(String strInval)
        {
            String cExp1 = "";
            String cExp2 = "";
            int c = 0;
            long lExp1;
            long lExp2;
            long lPower = 0;
            String strResult = "";

            for (c = 0; c < strInval.Length; c++)
            {
                if (c > 0)
                {
                    cExp1 = strInval.Substring(c - 1, 1);
                    cExp2 = strInval.Substring(c, 1);
                    //lExp1 = long.Parse(cExp1);
                    lExp1 = Convert.ToInt32(cExp1);

                    lExp2 = long.Parse(cExp2);
                    lPower = Raise(lExp1, lExp2);

                    strResult += lPower.ToString();
                }
            }

            return strResult;
        }

        public String RepeatExpand(String strInval, int Length)
        {
            String strResult = "";
            String strNewResult = "";
            strResult = Expand(strInval);

            while (strResult.Length < Length)
            {
                strNewResult = Expand(strResult);
                strResult = strNewResult;
            }

            return strResult;
        }


        public String GenerateKey(String strPassword)
        {
            int a = 0;
            int curByte = 0;
            int bcurByte = 0;
            int lChecksum = 0;
            //char[] bPassword = strPassword.ToCharArray();
            byte[] bPassword = System.Text.ASCIIEncoding.ASCII.GetBytes(strPassword);

            String strNumerics = "";

            // Now we need to calculate a checksum on the numerics
            for (a = 0; a < strPassword.Length; a++)
            {
                curByte = bPassword[a];
                lChecksum += curByte;
            }

            for (a = 0; a < strPassword.Length; a++)
            {
                curByte = bPassword[a];
                curByte += lChecksum;

                bcurByte = curByte % 100;
                bPassword[a] = Convert.ToByte(bcurByte);
            }

            for (a = 0; a < strPassword.Length; a++)
            {
                strNumerics += bPassword[a].ToString();
            }

            strNumerics = RepeatExpand(strNumerics, 512);

            return strNumerics;
        }


        public int GenRand(int Max)
        {

            return rnd.Next(Max);
        }

        public String EncryptText(String strPassword, String strPlaintext)
        {
            int b = 0;
            String strKey = GenerateKey(strPassword);

            byte[] bPlaintext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);
            byte[] bEnctext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);

            byte curbyte = 0;


            String strCurtrans = "";
            String strEncrypted = "";
            String strBase64 = "";
            int curTrans = 0;

            for (b = 0; b < strPlaintext.Length; b++)
            {
                strCurtrans = strKey.Substring(b, 3);
                curTrans = Int32.Parse(strCurtrans);
                curbyte = bPlaintext[b];

                for (int c = 0; c < curTrans; c++)
                {
                    curbyte++;
                }

                //curbyte += Convert.ToByte (curTrans);

                bEnctext[b] = curbyte;
            }

            strEncrypted = System.Text.ASCIIEncoding.ASCII.GetString(bEnctext, 0, bEnctext.Length);
            strBase64 = "#CS" + Convert.ToBase64String(bEnctext);

            return strBase64;
        }

        public String EncryptTextEx(String strPassword, String strPlaintext)
        {
            // Same encryption function with the additional feature of Byte randomisation feature
            // which is a part of the full cybercede algorithm.
            int b = 0;
            String strKey = GenerateKey(strPassword);

            byte[] bPlaintext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);
            byte[] bEnctext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext + "0"); // Add the zero to ensure we have space for the random byte

            byte curbyte = 0;

            // Now we need a random number between 1 and 47 for key cipher randomness
            int rand = GenRand(47);

            byte brand = Convert.ToByte(rand);

            String strCurtrans = "";
            String strEncrypted = "";
            String strBase64 = "";
            int curTrans = 0;

            for (b = 0; b < strPlaintext.Length; b++)
            {
                strCurtrans = strKey.Substring(b + rand, 3); // get a value from the key with a random offset.
                curTrans = Int32.Parse(strCurtrans);
                curbyte = bPlaintext[b];

                for (int c = 0; c < curTrans; c++)
                {
                    curbyte++;
                }

                //curbyte += Convert.ToByte (curTrans);

                bEnctext[b + 1] = curbyte; // Add 1 to offset the data from the random byte
            }

            // Now place our random byte at index 0 of the byte array
            bEnctext[0] = brand;

            strEncrypted = System.Text.ASCIIEncoding.ASCII.GetString(bEnctext, 0, bEnctext.Length);
            strBase64 = "#CT" + Convert.ToBase64String(bEnctext);

            return strBase64;
        }


        public String DecryptText(String strPassword, String strPlaintext)
        {
            int b = 0;
            String strKey = GenerateKey(strPassword);

            //byte[] bPlaintext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);
            //byte[] bEnctext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);

            byte[] bPlaintext = Convert.FromBase64String(strPlaintext.Substring(3));
            byte[] bEnctext = bPlaintext;

            byte curbyte = 0;


            String strCurtrans = "";
            String strEncrypted = "";
            String strBase64 = "";
            int curTrans = 0;

            for (b = 0; b < bPlaintext.Length; b++)
            {
                strCurtrans = strKey.Substring(b, 3);
                curTrans = Int32.Parse(strCurtrans);
                curbyte = bPlaintext[b];

                for (int c = 0; c < curTrans; c++)
                {
                    curbyte--;
                }

                //curbyte += Convert.ToByte (curTrans);

                bEnctext[b] = curbyte;
            }

            strEncrypted = System.Text.ASCIIEncoding.ASCII.GetString(bEnctext, 0, bEnctext.Length);

            return strEncrypted;
        }

        public String DecryptTextEx(String strPassword, String strPlaintext)
        {
            int b = 0;
            String strKey = GenerateKey(strPassword);

            //byte[] bPlaintext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);
            //byte[] bEnctext = System.Text.ASCIIEncoding.ASCII.GetBytes(strPlaintext);

            byte[] bPlaintext = Convert.FromBase64String(strPlaintext.Substring(3));
            byte[] bEnctext = bPlaintext;

            int rand = bPlaintext[0]; // Random offset of key stored as first byte in encrypted message

            byte curbyte = 0;


            String strCurtrans = "";
            String strEncrypted = "";
            String strBase64 = "";
            int curTrans = 0;

            for (b = 0; b < bPlaintext.Length - 1; b++) // -1 of length as we don't want to account for length of random byte
            {
                strCurtrans = strKey.Substring(b + rand, 3);
                curTrans = Int32.Parse(strCurtrans);

                curbyte = bPlaintext[b + 1]; // Offset of 1 to skip past the random byte

                for (int c = 0; c < curTrans; c++)
                {
                    curbyte--;
                }

                //curbyte += Convert.ToByte (curTrans);

                bEnctext[b] = curbyte;

            }

            strEncrypted = System.Text.ASCIIEncoding.ASCII.GetString(bEnctext, 0, bEnctext.Length - 1);

            return strEncrypted;
        }
    }
}
